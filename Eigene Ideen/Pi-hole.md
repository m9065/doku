# Dokumentation Pi-hole

 - Datum: 01.04.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist Pi-hole?
[Pihole](https://pi-hole.net/) ist ein Programm, welches mehrere nützliche Funktionen wie die eines Adblockers, DNS oder auf Wunsch sogar DHCP Servers ineinander vereint. Pihole fungiert als lokaler DNS Server, sofern man dies den Clients mitgeteilt hat, alle Anfragen die nach aussen wollen, gehen somit übers Pihole wo dann eine "Filterung" passiert. Anfragen die Blockiert werden (passiert automatisch, kann aber noch genauer konfiguriert werden), wie z.B. für Werbung verlassen das Netzwerk gar nicht erst, alle anderen Queries die in Ordnung sind, werden an einen upstream DNS wie z.B. `8.8.8.8` forwarded.
In einem zweiten Schritt werden wir Pihole eventuell noch als Rekursiven DNS Server einrichten, er wird sich dann also direkt beim provider der Seite alle Infos holen, und der Upstream schritt fällt sozusagen weg. Dazu kommt noch dass er Seiten auf denen er schonmal war im Cache speichern wird, was unter umständen die Performance verbessert.

![Pihole Netzwerkdiagramm](img/Pihole_Dia1.PNG) 

## Idee & Ziele
Werbung kann ganz schön nervig sein, wäre doch schön wenn man sie blockieren könnte, hat sich wohl der Erfinder des ersten Adblockers gedacht. Heutzutage ist das dank Browser-Extensions wie z.B. Ublock Origin kein Problem mehr, der einzige Nachteil den diese haben ist das man sie auf jedem Gerät, in jedem Browser separat hinzufügen muss. Dies ist sicherlich kein grosser aufwand, doch verglichen mit einem Pihole was pro Gerät nur einmal definiert werden muss, doch schon etwas mühsamer. Je nachdem ist es sogar möglich dem Router zu sagen, dass er allen das Pihole, statt seiner selbst, als DNS mitgibt, somit hätte man mit nur einer Konfiguration einen Adblocker für sämtliche Geräte im Netz, dazu zählen auch Handys etc. auf denen es sich normalerweise etwas schwieriger gestaltet Adblocker einzurichten.
Die ganze Adblock geschichte mal bei Seite, gibt es noch viele andere Features die wir gerne ausprobieren würden, wie etwa das einrichten von A Records, so dass man auf Dienste wie den [RPI-Monitor](https://gitlab.com/m9065/doku/-/blob/main/Arbeitsauftr%C3%A4ge/04_RPI-Monitor.md) nicht mehr nur über die IP, sonder auch über einen von uns Festgelegten Hostnamen erreichen kann. Was sicher auch noch spannend sein könnte, sind Black respektive Whitelists, mit denen gewisse Webseiten gezielt blockiert, oder erlaubt werden können.
Unsere konkreten Ziele sind:
- Pihole ist auf Raspberry Pi installiert
- Laptops verwenden den Raspberry Pi als DNS Server
- Mindestens eine Website ist via Blacklist blockiert, und somit nicht mehr zugänglich, z.B. https://www.tbz.ch
- Mindestens ein DNS-Record wurde definiert, z.B. für RPI-Monitor
- Pihole ist als Rekursiver DNS konfiguriert (*Optional*)
- Sämtliche Installations & Konfigurationsschritte sowie die konzepte dahinter wurden Dokumentiert, soll für jeden Nachmachbar sein
## Anleitung
### Vorbereitung
Damit Pihole zuverlässig funktionieren kann, ist es notwendig eine Statische IP Adresse zu konfigurieren. Dazu verbinden wir uns über VNC, und rechtsklicken oben auf das Internet Symbol, wo übers Kontextmenü die Option `Wireless & Wired Network Settings` geöffnet werden kann.
![ETH uplink](img/Pihole_Link.PNG)

Dort wählt man dann das gewollte Interface aus, auf der die Statische Adresse eingerichtet werden soll, in unserem Fall eth0, da wir den Pi übers Kabel mit dem Netz verbunden haben. Dann definieren wir die IP welche verwendet werden soll, hier `192.168.178.10` und den Standartgateway, beziehungsweise den Router, die anderen Felder lassen wir leer.
![StaticIP](img/Pihole_Static.PNG)

Somit war es das auch schon, der Pi sollte nun immer, auch nach einem Neustart ein paar Tage später die .10er Adresse zugewiesen bekommen.

### Installation Pihole
Die Installation von Pihole ist erstaunlich einfach, auf der [Website](https://pi-hole.net/) werden verschiedene Wege für verschiedene Systeme, unter anderem auch Docker, beschrieben, der Empfohlene besteht gerade mal aus diesem Einzeiler:
#### `curl -sSL https://install.pi-hole.net | bash`

Führt man ihn aus muss man ein ganz kleines bisschen Geduld haben, ehe ein TUI erscheint:

![TUI](img/Pihole_TUI.PNG)

Dieses führt einem, selbsterklärend, durch den kompletten Installationsprozess, man kann dabei im Grunde genommen nichts falsch machen wenn man alles auf den empfohlenen Standartwerten lässt, solange, wie zuvor beschrieben, eine Statische IP vergeben wurde, die sich nicht mehr ändert. 

Ist die Installation durchgelaufen, begrüsst einem ein solches Fenster:

![Success](img/Pihole_Success.PNG)

Bestätigt man hier mit OK gelangt man zurück auf die Konsole. Von dort aus sollte mit dem Befehl `pihole -a -p` noch ein Admin Passwort gesetzt werden, damit es einem Später leichter fällt sich im Webgui anzumelden.

### Konfiguration Webgui
Das Webgui ist über `<IP-Adresse>/admin` erreichbar, zuerst werden noch nicht soviele Optionen angezeigt, sobald man sich aber anmeldet, werden dem Menü Links noch einige Einträge mehr hinzugefügt, und das ganze sieht in etwa so wie auf dem unteren Bild aus:
![Webgui](img/Pihole_Webgui.PNG)

Oben Links hat man erstmal Systeminformationen wie z.B. Temperatur oder Memory Usage eingeblendet, während rechts davon einige Statistiken zu Pihole allgemein angezeigt werden. Man sieht wieviele Anfragen schon total über Pihole gegangen sind, wieviele davon Blockiert wurden, was diese Zahl in Prozent bedeutet, und wieviele Domains über eine Blocklist blockiert werden. Weiter unten gibt es dann noch ein paar andere Statistiken, der spannendste Teil befindet sich aber definitiv am Rand.

Dort können unter `Group Management -> Adlists` weitere Adlisten hinzugefügt werden, also Listen die Werbe-URLs enthalten, welche man möglichts blockieren will. Standartmässig ist schon eine Konfiguriert mit der man ganz gut aufgestellt ist, wir fügen zur Sicherheit aber noch zwei Weitere hinzu. Die Links zu den Listen können z.B. [hier](https://firebog.net/) oder allgemein im Internet nachgelesen werden.
Hat man eine solche Liste hinzugefügt muss auf dem Pi selbst noch der Befehl `pihole -g` ausgeführt werden, nur so kapiert das System dass neue Listen vorhanden sind, und aktualisiert sich dementsprechend.
![Blocklist](img/Pihole_Blocklist.PNG)

Als nächstes gehen wir zum Punkt `Blacklist` und tragen zu testzwecken die Domain `20minuten.ch` ein. Neben einer Exakten Domain könnte man auch nach RegEx Muster filtern, und so z.B. alle `.ch` Domains sperren, oder auch sonst alles mögliche zu machen. Wenn wir alles richtig eingestellt haben erscheint oben rechts ein grünes Popup, und der folgende Eintrag ist zu sehen:
![Blacklist](img/Pihole_Blacklist.PNG)

Zuletzt legen wir unter `Local DNS -> DNS Records` noch einen neuen Record für das Pihole an. Dieser hört auf den Namen `pi.hole` und leitet anfragen an unsere IP `192.168.178.10` weiter. Somit muss man nicht jedes mal die IP eingeben wenn man aufs Pihole Dashbaord möchte, sondern kommt auch bequem über die URL darauf.
![A-Record](img/Pihole_Record.PNG)

### Client verbinden
Einem Endgerät den DNS Server mitzugeben ist, zumindest unter Windows, keine Hexerei. In den Adapteroptionen kann man unter Eigenschaften -> IPv4 die Adress eintragen und mit Ok wieder alle fenster schliessen.

![Client_Verbinden](img/Pihole_Client.PNG)

Zur kontrolle würde ich die Eingabeaufforderung als Administrator öffnen, und diese Befehle ausführen, um zu testen, ob der DNS Server tatsächlich geschluckt wurde.
```cmd
ipconfig /flushdns
ipconfig /flushdns
ipconfig /all
```
Nach `ipconfig -all` erscheint eine lange liste, mit diversen Informationen zu allen auf dem System vorhandenen Adaptern. Hier ist nur derjenige interessant, auf dem im vorherigen schritt auch der DNS spezifiziert wurde, für mich also auch wieder Ethernet.

![ipconfig](img/Pihole_Ipconfig.PNG)

Wenn wie auf dem Bild nur ein einziger eintrag erscheint, hat man alles richtig gemacht, und kann sich sicher sein dass Pihole als DNS Server verwendet wird.

Eine etwas radikalere möglichkeit um gleich auf einen schlag **alle** Clients zu "verbinden", wäre es im Router festzulegen dass der Standardmässige DNS das Pihole sein soll. Bei der Fritzbox würde das ganze etwa so aussehen, ich werde dies jedoch nicht ausprobieren, da ich mit meinem Heimrouter lieber etwas vorsichtig umgehe, und so wenig wie möglich Verändere.
![Router](img/Pihole_Router.PNG)

## Test
### Werbung
Um zu schauen ob wir immernoch Werbung bekommen, oder ob die Adblock funktionalität erfolgreich gewährleistet werden kann, besuchen wir einige Seiten die normalerweise ziemlich voll sind mit Werbung. Ein Beispiel dafür wäre https://speedtest.net
Normalerweise wären die Ränder mit Anzeigen gefüllt, was jetzt allerdings nicht mehr der fall ist, es scheint also schonmal geklappt zu haben
![Speedtest](img/Pihole_Speedtest.PNG)

Um alles nochmals etwas genauer verifizieren zu können besuchen wir eine zweite Website, https://20minuten.ch
Hier gibt es zwar auch keine Werbung, man sieht aber trotzdem die Stellen, an denen eigentlich welche sein müsste, sie wird einem nur nichtmehr angezeigt. Das liegt vermutlich daran dass derjenige der die Seite gebaut hat, extra Platz für Werbung reservierte, die zwar nicht mehr darüber geladen wird, der Platzhalter wird aber logischerweise nicht als Werbung erkannt, und somit auch nicht blockiert.
![20min](img/Pihole_20min.PNG)

Noch schlechter steht es um Streaming Dienste wie [Youtube](https://www.youtube.com/) oder [Spotify](https://www.spotify.com/de/). Dort bekommt man die volle Ladung Werbung ab, und man merkt nichts davon dass der ganze Traffic über Pihole geht.

### Blacklist
Wie auf dem Bild zu sehen ist die zuvor geblockte [Website](https://20minuten.ch) nicht mehr erreichbar, der Blacklist eintrag war also erfolgreich.
[siteNotFound](img/Pihole_Blackblock.PNG)

### DNS Record
Das mit dem DNS Record hat ebenfalls geklappt, das Webgui ist jetzt entweder über `192.168.178.10` oder einfach `http://pi.hole` erreichbar.
![Pihole](img/Pihole_Pihole.PNG)

## Recursive DNS
### Was ist das?
Im gegensatz zur Iterativen DNS abfrage, bei der der Client direkt mit allen involvierten DNS Servern spricht, hat mein bei der Rekursiven Variante nur kontakt zu einem einzigen DNS Server, welcher dann den kompletten Namen für einen Auflöst. Bildlich kann man sich das etwa so vorstellen:
![RecursiveDNS](img/Pihole_Recursive.PNG)

### Setup
Um dieses Ziel zu erreichen müssen wir zuerst ein Programm namens `Unbound` installieren. Das geht folgendermassen:
```shell
sudo apt update
sudo apt install unbound
```

Anschliessend zur installation müssen wir gemäss [Template](https://docs.pi-hole.net/guides/dns/unbound/) ein Configfile erstellen, und den Inhalt aus der Anleitung übernehmen. Sobald es das File gibt kann man den Dienst auch gleich starten und aktivieren, so dass er beim Booten vom Raspi immer gleich automatisch hochkommt. Das kann mit diesen Befehlen gemacht werden:
```shell
sudo nano /etc/unbound/unbound.conf.d/pi-hole.conf
sudo systemctl start unbound
sudo systemctl enable unbound
```
Einfügen geht mit Rechtsklick, zum Speichern benutzt man in nano **Ctrl+o** und **Ctrl+x** um den Editor wieder schliessen zu können.

Neben den Konfigurationen auf dem Pi selbst müssen auch im Webgui noch einige kleinere anpassungen vorgenommen werden. Diese finden sich, wie der Name bereits vermuten lässt, unter `Settings -> DNS`. Dort können die Haken vom aktuellen DNS Server entfernt werden, wenn man zuvor google hatte, sollte es jetzt so aussehen:
![Google](img/Pihole_Google.PNG)

Anschliessend trägt man im Fenster rechts unter `Custom 1 (IPv4)` den Server `127.0.0.1#5335` ein, und speichert ganz unten alles durch bestätigen der Save taste.

Im Query Log sieht man dann auch gleich dass die Änderungen erfolgreich waren, sobald man eine Webseite öffnet:
![RecursiveDNS](img/Pihole_RecursiveSuccess.PNG)

## Probleme
Kurzzeitig hatten wir ein Problem mit der White/Blacklist funktion, da es uns nicht mehr möglich war Einträge hinzuzufügen. Oben rechts ist jeweils eine rote Fehlermeldung aufgetaucht die in etwa sagte dass wir keine Rechte haben in die Datenbank davon zu schreiben. Nach einem Reboot vom Pi war immernoch dasselbe Problem vorhaden, bis wir im Internet fündig wurden. Es gibt nämlich einen Befehl der `pihole -r` heisst, über welchen man seine Installation rekonfigurieren kann. Es gibt zwei Optionen wovon eine "Reparieren" heisst. Diese haben wir dann durchlaufen lassen, und alles bezüglich White/Blacklists hat wieder funktioniert.
Ein Weiteres "Problem" wenn man dem so sagen kann, ist das die Werbung nicht überall verschwindet, und manchmal nur zum Teil. Ehrlichgesagt haben wir uns das etwas besser vorgestellt, aber da lässt sich halt nichts machen, wir sind trotzdem zufrieden mit dem mini-Projekt, vor allem im hinblick auf die DNS Funktionen, die sich als weitaus mächtiger herausgestellt haben, als wir eigentlich annahmen.
Insgesamt haben wir vielleicht **5 Stunden** investiert. Die installation an sich war zwar sehr schnell vorbei, aber sich mal mit den ganzen DNS Themen auseinanderzusetzen, für die wir schlussendlich auch bei Pihole geblieben sind, und vor allem auch die Dokumentation zu schreiben, ging doch etwas länger.

## Quellen
- https://pi-hole.net/
- https://docs.pi-hole.net/guides/dns/unbound/
- https://www.youtube.com/watch?v=FnFtWsZ8IP0
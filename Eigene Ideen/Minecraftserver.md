<!-- Das ist ein Kommentar -->
# Dokumentation Minecraft Server

 - Datum: 16.04.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist Minecraft?

![Minecraftlogo](img/Minecraft_Logo.PNG)

Das im Jahre 2011 erschienene Videospiel Minecraft sollte zwar den meisten bekannt sein, wir wollen aber trotzdem nochmal kurz darauf eingehen. Im klassischen [Minecraft](https://www.youtube.com/watch?v=MmB9b5njVbA) wird man als Spieler in irgendeiner zufällig Generierten Welt ausgesetzt, in der alles nur aus Blöcken besteht. In dieser Sandbox ist die eigene Vorstellungskraft das einzige Limit, man kann alleine oder gemeinsam mit anderen Spielern die Welt erkunden gehen, kann verschiedene Ressourcen Sammeln, daraus dinge herstellen, häuser bauen und so weiter.
Später wurden dann auch Server eröffnet, auf denen es Spielmodi gibt, die vom eigentlichen Game nie vorgesehen waren, die Community bastelte Mods rund um Minecraft, und das spiel wuchs mit der Zeit generell immer weiter, bis wir heute an einem Punkt sind, wo es so gut wie jeder kennt.

## Idee & Ziele
Da wir selber gerne Minecraft Spielen lag der Gedanke selber einen kleinen Server aufzuziehen nicht weit weg. Dass dies, unter Linux, relativ leicht zu bewerkstelligen ist, fanden wir heraus als wir im Internet nach möglichen lösungsansätzen suchten, Minecraft selber hat sogar eine [Website](https://www.minecraft.net/de-de/download/server) auf der sie einem alle nötigen Ressourcen zur verfügung stellen, um einen Server aufzubauen.

Unsere Ziele sind:
- Server wurde gemäss anleitung installiert und konfiguriert
- Settings sind auf Serverebene einstellbar
- Der Server ist aus dem LAN erreichbar
- Mindestens 2 Spieler können gleichzeitig darauf spielen
- Sämtliche Schritte wurden so dokumentiert dass alles gut verständlich & jederzeit nachbaubar ist


## Anleitung
### Installation Java
Da Minecraft eine in Java geschriebene Applikation ist, wird vorausgesetzt dass diese Sprache auf dem System installiert ist.

Eigentlich würde man dazu mit `sudo apt search openjdk` nach einer passenden Version suchen und diese nachher z.B. mit `sudo apt install jdk-8-jdk-headless` installieren, aber seitdem die Minecraft Version **1.18.2** verfügbar ist, wird vorausgesetzt dass java 17 installiert ist, eine Version der Programmiersprache, die der apt Paketmanager noch nicht kennt, der Download läuft also etwas komplexer ab.

Am besten erstellt man sich zuerst einen Ordner, in dem die ganzen Minecraft Files gelagert werden können. 

```shell
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo os=raspbian dist=buster bash
```

Jetzt kann auch schon Java 17 installiert werden, dazu lädt man sich einen Komprimierten Ordner herunter, entpackt diesen, und kann Java dann auch schon ausführen, wenn auch etwas anders als gewohnt.

```shell
sudo wget https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.1%2B12/OpenJDK17U-jdk_arm_linux_hotspot_17.0.1_12.tar.gz
tar xzfv OpenJDK17U-jdk_arm_linux_hotspot_17.0.1_12.tar.gz
```
Mit diesem Befehl überprüft man dann ob die installation geklappt hat, java muss jetzt übrigens immer über `./jdk-17.0.1+12/bin/java` gestartet werden, ein einfaches `java` reicht nichtmehr.

```shell
./jdk-17.0.1+12/bin/java -version
```

### Installation Minecraft Server
Die installation des Servers ist jetzt recht simpel, von dieser [Website](https://www.minecraft.net/de-de/download/server) kopiert man sich einfach die adresse des links zur jar datei, und lädt sich diese dann auf den Raspberry Pi.

```shell
sudo wget https://launcher.mojang.com/v1/objects/c8f83c5655308435b3dcf03c06d9fe8740a77469/server.jar
```

Jetzt kann der Server auch schon mit diesem Befehl gestartet werden, dieser sagt lediglich dass java die server.jar datei ausführen soll, dass er dazu 2 GB RAM zur verfügung hat, und dass das ganze ohne gui gestartet werden soll:

```shell
sudo ./jdk-17.0.1+12/bin/java -Xmx2048M -Xms2048M -jar server.jar nogui
```

Obwohl unser Raspberry Pi eigentlich 8 Gigabyte Arbitsspeicher hätte, sind wir aufgrund des 32bit Betriebssystems auf 2GB Limitiert. Dies ist zwar nicht ganz optimal, aber wir lassen uns dadurch nicht aufhalten.

Wenn man den Befehl abgeschickt hat, werden zuerst einige Files generiert, unter anderem auch die **EULA**. Dieser muss zuerst zugestimmt werden, ehe man vortfahren kann, das geht wie folgt.

```shell
sudo nano eula.txt
eula=true
```

Wenn der wert auf true gesetzt wurde kann man man den Befehl nochmals abschicken, und der Server startet:

![start](img/Minecraft_start.PNG)

Dies dauert beim ersten mal recht lange, aber sobald der Server bereit ist, kann man sich in Minecraft über die IP Adresse vom Raspi verbinden, dazu aber später mehr.

![start_finished](img/Minecraft_sf.PNG)

### Konfiguration Minecraft Server
Um eigene Konfigurationen vorzunehmen müssen wir den server nochmals stoppen. Dazu in der Serverkonsole einfach den Befehl `stop` eingeben, und warten bis alles beendet wurde.

Jetzt kann man auf die Datei server.properties zugreifen:

```shell
sudo nano server.properties
```

und dort einige anpassungen vornehmen, man kann z.B. einstellen wie gross die welt sein soll, wieviele spieler erlaubt sind, ob pvp möglich ist und so weiter. Zu testzwecken werden wir die Message of the day definieren:

```
motd=Raspberry Pi Minecraft Server
```

Jetzt erstellen wir noch ganz kurz ein bash script über das wir den Server starten können, auf diese weise müssen wir uns nicht immer den Langen befehl merken:

```shell
sudo nano start.sh
#!/bin/bash
sudo ./jdk-17.0.1+12/bin/java -Xmx2048M -Xms2048M -jar server.jar nogui
Ctrl + o, Ctrl + x
sudo chmod +x start.sh
./start.sh
```


## Test
Sobald der Server wieder da ist, gehen wir in den Minecraft Launcher, starten das Spiel, und wählen unter Multiplayer den Button **Add Server** wo wir daraufhin die IP vom Raspi eingeben:

![connect](img/Minecraft_connect.PNG)

Wenn man jetzt etwas wartet sieht man auch gleich dass die zuvor definierte motd übernommen wurde:

![motds](img/Minecraft_motds.PNG)

Sobald man die Welt nun betritt, ist das auch auf der Serverkonsole zu erkennen:

![join](img/Minecraft_join.PNG)

Im Spiel sieht die welt jetzt z.B. so aus, wir haben es also geschafft:

![game](img/Minecraft_game.PNG)

## Probleme
<!-- Nur 2 giga ram, java 17 für neusti mc version -->
Wir hatten zuerst Probleme damit den Server zu starten weil wir noch kein Java 17 installiert hatten, und nicht wussten dass dies Notwendig ist. Im Internet hatten aber relativ viele leute dasselbe Problem und wir konnten die entsprechende Version problemlos nachinstallieren.

Ansonsten ist es natürlich etwas ärgerlich dass wir gerade mal einen Viertel des uns zur verfügung stehenden RAMs nutzen können, aber was will man machen, das spiel läuft trotzdem realtiv flüssig, auch wenn teilweise noch ein paar verzögerungen mitdrin sind, vor allem dann wenn blöcke abgebaut werden.

Insgesamt sind wir ziemlich überrascht davon wie leicht diese Aufgabe zu bewerkstelligen war, wir haben für alles zusammen vielleicht **2-3 Stunden** Zeit aufgewendet.

## Quellen
- https://www.youtube.com/watch?v=LrugHAcgLz4
- https://www.reddit.com/r/raspberry_pi/comments/r677q9/installing_java_17_on_pi_4/
- https://www.minecraft.net/de-de/download/server
- https://www.minecraftforum.net/forums/support/server-support-and/3129761-1-18-server-error
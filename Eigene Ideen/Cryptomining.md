<!-- Das ist ein Kommentar -->
# Dokumentation Cryptomining

 - Datum: 15.04.2022
 - Name: Tim Schefer, Justin Barthel

## Was sind Kryptowährungen?
Um zu verstehen was eine Kryptowährung tatsächlich ist, ist es wichtig zu wissen, welche Ziele man mit einer solchen erreichen will. Das wichtigste ist sicher mal dass man ein Dezentrales System aufbauen, und somit ein Geldsystem erschaffen möchte, dass nicht von einer einzigen Partei kontrolliert werden kann, wie es bei [FIAT](https://de.wikipedia.org/wiki/Fiatgeld) der fall ist. Unter FIAT versteht man übrigens "normales Geld" also so etwas wie Euro, Schweizer Franken oder US Dollar. Diese Währungen haben alle den Nachteil dass die Zentralbanken soviel davon drucken können wie sie wollen, das Geld wird mit der Inflation also immer weniger wertvoll. Gewisse Kryptowährungen sind Limitiert um dem vorzubäugen, der Bitcoin z.B. auf 21 Millionen, allgemein versucht man denselben ansatz den auch schon Gold verfolgt zu nehmen, und das ganze zu Digitalisieren so dass es für die Heutige Zeit passt.
Hier kommt jetzt eine Technologie namens [Blockchain](https://de.wikipedia.org/wiki/Blockchain) zum einsatz, eine solche ist im Prinzip nichts anderes als eine riesige Liste, in der alle Transaktionen festgehalten werden, die jemals mit einer bestimmten Kryptowärung gemacht wurden. Das coole an dieser "Liste" ist, dass sie jeder lokal bei sich zuhause haben kann, und ihre Echtheit garantiert ist, ohne irgendeine Zentrale Instanz zu haben. 

![Crypto](img/Mining_ToM.PNG)

## Ablauf Miningprozess
Während Fiat Währungen aus dem nichts erschaffen werden können, muss für die entstehung von gewissen Kryptowährungen ein Prozess durchlaufen werden, dieser nennt sich Proof of Work. Hier kommen jetzt die Miner ins Spiel, welche dieses Verfahren durchführen, und als Belohnung dafür ein bisschen Geld bekommen. Um zu verstehen wie Proof of Work funktioniert muss einem klar sein dass Krypto transaktionen immer in Blöcken mit einer fixen grösse zusammengefasst werden, als Miner hat man nun die Aufgabe diese Blöcke zu "validieren", und zu checken ob sie ohne Risiko in die Blockchain aufgenommen werden können.
Das ganze hat seinen Ursprung in den anfängen des Emails, als man damit versuchte Spamer aufzuhalten, in dem sie mit ihrem Computer jedes Mal eine kleine Berechnung durchführen mussten, bevor ein Mail versendet wurde. Dabei entstanden so hohe Stromkosten etc. Dass es sich für Spamer nicht mehr lohnte unzählige Mails zu versenden, weil sie damit weniger einnahmen generierten als sie an Ausgaben hatten.
Wie gesagt hat man dieses Konzept dann auf Krypto angewandt, um sicherzustellen dass alle Transaktionen in einem Block legitim sind. Damit garantiert jeder dieselbe Blockchain hat, werden Blöcke mit hohen Transaktionskosten bevorzugt.

![Zeichnung](img/Mining_Zeichnig.PNG)
1. Miner bekommt einen Block, mit der Aufgabe, eine gehashte Zahl zu finden die z.B. mit 30 0en startet.
2. Das zu machen ist aufwendig, und braucht viel Rechenleistung, aber andere Miner können einfach überprüfen ob die richtige Zahl gefunden wurde.
3. Jetzt kann der Block in die Blockchain aufgenommen werden, und zwar werden dabei Blöcke mit einer hohen Transaktionsgebühr immer bevorzugt bearbeitet, damit garantiert jeder dieselbe Reihenfolge in seiner Blockchain hat.

Als Minder wird man dafür belohnt die Passende Zahl zu einer gewissen Vorgabe gefunden zu haben, weil der mit Transaktionen gefüllte Block, so ohne Risiko in die Blochchain aufgenommen werden kann, der Aufwand würde sich für Betrüber etc. nicht lohnen, und alle Garantiert dieselbe Blockchain haben, man braucht keine Zentrale Stelle. Jeder weiss auf diese Art und Weise immer, wer wem wieviel schuldet, hinter den- Zahlen steckt kein echter Wert, aber man kann trotzdem damit Handeln, ist gleich wie mit CHF.

## Idee & Ziele
Kryptowährungen sind in den letzten paar Jahren ziemlich durch die Decke gegangen, und wie so ziemlich jeder, haben natürlich auch wir davon mitbekommen. Uns faszinierte nicht nur die Möglichkeit dadurch Geld zu verdienen, wir waren sofort auch von den ganzen Konzepten und der Technik dahinter begeistert, die dafür sorgt dass Crypto so funktioniert wie es funktioniert.
Obwohl wir uns in der Vergangenheit bereits ein wenig mit dem Thema auseinandergesetzt haben, würde ich uns niemals anmassen, dass ganze Thema vollumfänglich zu verstehen, das tun glaube ich die allerwenigsten, aber wir wollen es mit diesem "Projekt" wenigstens versuchen. Um dies zu bewerkstelligen werden wir die Kryptowährung Monero minen, da der Algorithmus welcher beim Mining von dieser zum Zug kommt verhältnissmässig Ressourcenschonend und effizient arbeitet. Aufgrund der geringen Rechenleistung vom Raspi rechnen wir nicht damit grossartige gewinne zu erwirtschaften, mal schauen ob dabei etwas herausspringt ;)

- [xmrig](https://xmrig.com/) ist als Miningsoftware auf dem Raspi installiert
- Es kann nach [Monero](https://www.getmonero.org/) geschürft werden
- Wallet wurde erstellt
- Statistiken sind in einem Dashboard ersichtlich
- Raspi wurde Übertaktet (optional)
- Alle Schritte sind sauber dokumentiert, und jederzeit nachbaubar


## Anleitung
### Vorbereitung
#### Wallet erstellen
Damit man überhaupt eine Chance hat an seine Coins zu kommen, muss zuerst eine Wallet, also sozusagen ein Konto erstellt werden, sofern man noch keines hat. Dafür gibt es diverse anbieter, wir haben uns jedoch für die Ofizielle Monero GUI Wallet entschieden, da wir sowieso schon mit Monero arbeiten. Sie kann [hier](https://www.getmonero.org/downloads/) heruntergeladen werden.

![wallet](img/Mining_Wallet1.PNG)

Diese Wallet bietet einem die Wahl zwischen einem `simple` oder `Advanced Mode`. Letzteres beinhaltet neben den ganz normalen Funktionen unter anderem die Möglichkeit, sich direkt den aktuellen Monero Kurs, innerhalb der Wallet anzeigen zu lassen, man muss also nicht mehr danach suchen. Ausserdem kann man seine Hardware wallet, das ist grob gesagt, ein USB Stick, auf dem die Kryptowährungen gespeichert werden, damit Verknüpfen, muss aber mindestens 1/3 der gesamten Blockchain herunterladen, während man sich beim «Simple Mode» einfach mit jemandem Verbindet der die Blockchain hat, aber dann halt auch nur über die Basics, wie z.B. Geld Senden/Erhalten verfügt.
Wir entscheiden uns hier für den **simple Mode** da dieser für unsere zwecke reichen wird.

Jetzt ist es so dass man einmal ein ganz normales Passwort festlegen kann, mit dem man sich dann immer anmeldet, zu welchem man zusätzlich ein 25 Wörter Langen `Seed Phrase` bekommt. Dieser besteht aus 25 zufällig aneinandergereihten englischen Wörtern, und er erlaubt einem seine Wallet von überall aus Wiederherzustellen, solange man die Wörter in der Richtigen Reihenfolge kennt. Wenn man also ernsthaft vorhat in Krypto zu Investieren sollte man diese Wörter auf keinen Fall jemand anderem geben, mit ihnen erlangt man sozusagen die Vollmacht über das Konto.

![wallet2](img/Mining_Wallet2.PNG)


#### 64bit
<!-- ds64 shell -->
Soviel mal zur ganzen Theorie dahinter, jetzt wollen wir unser geplantes vorhaben auch auf dem Pi umsetzen. Wir haben Raspbian dummerweise in der 32bit Version installiert, die Mining Software, welche wir verwenden wollen, läuft allerdings nur auf 64bit Betriebssystemen. Eine Möglichkeit wäre es jetzt gewesen das OS nochmals neu aufzusetzen, dass wäre für uns allerdings sehr schmerzhaft gewesen, da wir noch viele andere Aufträge die wir noch nicht abgeben konnten, auf derselben SD-Karte haben, diese wären somit weg. Die andere Option, für die wir uns dann auch entschieden haben, ist es ein Programm namens `raspbian-nspawn-64` zu installieren. Mit diesem können wir eine art Container starten, in dem Simuliert wird, dass wir eine 64 bit umgebung haben, obwohl dem gar nicht so ist, die Installation klappt über folgende kommandos:

```shell
sudo apt update
sudo apt upgrade -y
sudo apt-get install -y raspbian-nspawn-64
```

der Container kann dann gestartet werden, alle weiteren schritte werden wir in diesem Tätigen.

```shell
ds64-shell
```

Dass das ganze geklappt hat, sieht man unter anderem auch daran dass sich der Hostname links im Terminal geändert hat:

![hostnamesuccess](img/Mining_HostnameSuccess.PNG)

### Installation xmrig
Wir werden mit der Miningsoftware xmrig arbeiten. Für diese müssen wir zuest ein paar dependencies herunterladen:
```shell
sudo apt-get install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev
```

Sobald diese heruntergeladen sind, kann das Repository geklont werden:
```shell
git clone https://github.com/xmrig/xmrig.git
```

Wenn dieser Befehl erfolgreich durchgelaufen ist, hat man den Sourcecode auf seinem rechner, die Applikation wird einem nämlich nicht schon Pfannenfertig serviert, sondern man muss den Code selber kompilieren.

```shell
cd xmrig
mkdir build
cd build
cmake ..
make
```

Dieser Prozess kann recht viel Zeit in anspruch nehmen, daher sollte man besser etwas geduldig sein, und nicht vorzeitig abbrechen. Ansonsten war es das auch schon von der installation, sie ist also gar nicht so kompliziert wie man zuerst vielleicht gedacht hat.

### Und jetzt?
#### Miner starten
<!-- Wie bediene, Dashboard, whattomine.com... -->
Der Miner ist zwar installiert, läuft allerdings noch nicht, das Programm kann folgendermassen gestartet werden:
```shell
./xmrig --donate-level 1 -o pool.supportxmr.com:3333 -u <WALLET> -p MoneroMiner
```

Wichtig zu bemerken ist, dass der Befehl nur funktioniert wenn man den Container mit `ds64-shell` betreten hat, und sich im verzeichnis `/home/pi/xmrig/build` befindet.
Mit `./xmrig` sagt man dass der xmrig miner gestartet werden soll, xmrig ist der Name des Programms. `--donate-level 1` legt fest wieviel von seinen Einnahmen man dem Pool spenden möchte, auf dem man aktuell am minen ist. Ein Pool kann man sich wie eine Goldmine vorstellen, um wieder die Gold analogie zu haben, damit man in ihr schürfen darf, zahlt man einen Preis. Wieviel man dabei abgibt wird in Leveln angegeben, 1 ist das Minimum. `-o pool.supportxmr.com:3333` sagt dem Miner, zu welchem Pool er sich begeben soll, und über Welchen Port das ganze abläuft. Mit dem Port ist es einem möglich zu spezifizieren, wie «schwierig» das mining an sich sein soll. Wenn man, so wie ich mit dem Raspi, nur sehr schwache Hardware zur Verfügung hat, sollte man den Port 3333 wählen, für etwas stärkere Komponenten gibt es noch die Möglichkeit 5555 anstelle dieses einzutragen, was dann bewirkt, dass man schwierigere Aufgaben bekommt, aber dafür auch höhere Renditen erzielen kann. Unter `-u` kann man dann noch seine Wallet Adresse angeben, das ist sozusagen die Kontonummer, wo das Geld dann hin «überwiesen» wird, und zu guter letzt kann man mit `-p MoneroMiner` seinem Arbeiter einen Namen zuweisen, wenn man also mehrere Sessions am laufen hat, und über alle per SSH/VNC verbunden ist, kann das ganz hilfreich sein, um zu erkennen welches Gerät zu welcher Sitzung gehört.

Schickt man den Befehl dann ab wird man von folgedem Interface begrüsst:

![startminer](img/Mining_startminer.PNG)

Wie man sieht, erhält man zuerst ein paar allgemeinere Infos, z.B. über die Version, was für Libraries benutzt werden, und was für Hardware vorhanden ist. Weiter unten erkennt man auf welchem pool «gearbeitet» wird, und man kann sich mit den Tasten `h`, `p`, `r`, `s`, und `c` einige Informationen über die aktuelle Hashrate, also auf gut Deutsch gesagt «Geschwindigkeit» anzeigen lassen, hat die Möglichkeit den Miner zu pausieren, beziehungsweise weitermachen zu lassen, und man kann sich Informationen über die Verbindung zum Pool anzeigen lassen anzeigen lassen. Was «results» genau bewirkt konnte ich leider nicht in Erfahrung bringen, da immer die Meldung «no any results yet» kam. Vermutlich ist das so weil man erst Resultate sieht, sobald man Geld überwiesen bekommen hat, das ist allerdings erst ab 0.004 Monero möglich, und für uns lässt es sich nicht bewerkstelligen innerhalb der Zeit die wir haben soviel zu minen.

#### Dashboard
Um sich alles Grafisch anzeigen zu lassen, gibt es ein [Dashboard](https://minexmr.com/dashboard). Auf der Website muss man lediglich seine Wallet adresse eingeben, und man bekommt zugriff darauf.

Zuoberst hat man dann auch gleich die drei wichtigsten Elemente.
Zuerst einmal sieht man wie gross die aktuelle Hashrate ist. In unserem Fall wird hier noch nichts angezeigt da das ganze etwas Zeit braucht um zu aktualisieren.

![yourhashrate](img/Mining_Dashboard1.PNG)


Dann gibt es eine Kachel auf der man sieht wieviel Monero oder kurz XMR schon geminet wurde, wir haben hier 0.000004 Monero, was zum Aktuellen Kurs etwa **0.087 Rappen** entspricht. Unten gibt es noch 2 Buttons für **Payments** und **Pay now**. Unter Payments sieht man alle auszahlungen die bereits getätigt wurden, in unserem Falle passiert nichts, da wir noch nicht genug schürfen konnten, um uns auszahlen zu lassen. Der Knopf Pay Now würde die überweisung in gang bringen, allerdings ist das erst ab einem Minimalbetrag von 0.004 XMR möglich.

![pendingrewards](img/Mining_Dashboard2.PNG)

![cantsend](img/Mining_Dashboard2.1.PNG)


Zuletzt ist dann noch die Hashrate des gesamten Pools zu sehen :

![poolhashrate](img/Mining_Dashboard3.PNG)

#### Whattomine
Auf der Website https://www.whattomine.com kann man ausrechnen wieviel Geld man mit einer bestimmten Hashrate innerhalb von 24h oder einer Woche machen würde wenn man einen bestimmten coin minet. Unsere Hashrate ist mit etwa 92 H/s nicht besonders gut, auf meinem Desktop PC erreiche ich im vergleich dazu etwa 27MH/s, umrechnen kann man die einheiten [hier](https://coinguides.org/hashpower-converter-calculator/)

![hashrate](img/Mining_Hashrate.PNG)


Bei Monero wird hier noch nicht einmal etwas angezeigt, die Nachkomastellen reichen nicht aus, und selbst wenn man von einer höheren Hashrate als 92 ausgeht, die wir realistisch gesehen, auf dem pi, niemals haben werde, geht es ewig, um endlich den notwendigen Betrag zu erreichen. Deshalb werden wir auch niemals in der Lage sein Monero effektiv auf der Wallet zu sehen.

![whattomine](img/Mining_Whattomine.PNG)

## Übertakten
### Was ist das?
Um dem Pi dann doch noch etwas mehr leistung zukommen zu lassen, wollen wir ihn Übertakten.

Allgemein lässt sich sagen, dass man dem Prozessor, oder generell der Komponente, die man Übertakten will, erlaubt mehr Strom zu ziehen, als eigentlich vorgesehen. Daraus resultiert, dass mehr Spannung anliegt, woraus, höhere Taktraten, und somit auch Geschwindigkeiten erzielt werden können. Man kann es sich ein bisschen wie bei den Autos vorstellen. Je schneller man fahren möchte desto mehr Benzin wird verbraucht, nur dass es in diesem Fall Strom anstatt Benzin, und die Taktrate in MHz anstatt Geschwindigkeit in km/h ist.

### Setup
Um dies zu bewerkstelligen muss man den Container mit `exit` nochmals kurz verlassen, da das Übertakten auf Globaler ebene stattfindet. 
Folgende Befehle sind dafür notwendig:

```shell
cd /boot
sudo nano config.txt
```

In der Datei ergänzt man dann die 2 auf dem Bild markierten Zeilen:

![OC](img/Mining_Overclock.PNG)

jetzt startet man den Raspi neu, und wartet kurz, bis er wieder da ist. Ob das ganze geklappt hat, sieht man wenn man über VNC verbunden ist, und über das Frequency symbol oben rechts in der Taskleiste Hovert.

![OCS](img/Mining_OCS.PNG)

## Probleme
<!-- Probleme und Zeitaufwand 64bit-->
Zwischendurch hatten wir probleme mit dem Miningprogramm, da wir ursprünglich ein anderes verwenden wollten, dann aber gemerkt haben dass die Installation von diesem nicht klappen will. Desshalb sind wir dann auf xmrig gewechselt, verloren zwar ein bisschen etwas an arbeit, aber konnten gut wieder alles zum laufen bringen. Insgesamt haben wir für den Auftrag vielleicht **4 Stunden** arbeit investiert, und sind mit dem ergebnis sehr zufrieden.


## Quellen
- https://minexmr.com/miningguide#:~:text=Payments,rewards%20are%20above%200.1XMR.
- https://www.youtube.com/watch?v=NMMiT_eMd-s
- https://schroederdennis.de/allgemein/raspberry-pi-kryptomining-monero-tutorial-howto-bitcoin-crypto-anleitung/
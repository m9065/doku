# Dokumentation VPN

 - Datum: 27.03.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist ein VPN?
VPN, oder ausgeschrieben **V**irtual **P**rivate **N**etwork ist eine Technologie die es einem erlaubt sich, von aussen, in ein anderes Netzwerk zu verbinden, obwohl man nicht physisch vor ort ist. Dabei wird eine art Tunnel zwischen dem Endgerät dass man Benutzen möchte und dem VPN Server aufgebaut. Die Kommunikation zwischen den beiden ist somit verschlüsselt und kann nicht direkt von potentiellen angreifern abgehört werden. Ist man dann beim VPN-Server und somit im Zielnetzwerk angekommen erhält man eine neue IP-Adresse aus diesem, kann auf Netzwerkdienste zugreifen, und wird allgemein wie ein ganz normaler Client behandelt.

![VPN Funktionsweise](img/VPN-Funktionsweise.PNG)

### Selfhosted vs Service (z.B. NordVPN)
Es gibt verschiedene arten von VPN', man kann zum einen bei Anbietern wie NordVPN ein Abo abschliessen, oder das ganze selbst hosten. Entscheidet man sich für die erste Variante bezahlt man in dem meisten Fällen einen fixen Betrag pro Monat, kann sich dafür aber in wesentlich mehr "Netzwerke" verbinden, während man bei der DIY Lösung nichts bezahlt aber sich dafür immer nur ins Heimnetzwerk verbinden kann. Diese Option bringt einem also effektiv nur etwas wenn man so tun möchte als wäre man Zuhause obwhohl man es gar nicht ist, vertraut man einem Service Provider kann man auch so tun als wäre man in einem anderen Land, um z.B. Geoblocking zu umgehen. 

### Wichtigste Use-Cases
- Sichere Verbindung in ein Netzwerk, obwohl man nicht vor Ort ist
- Ohne bedenken Öffentliche / Unsichere Internetzugänge nutzen
- Zugriff zu Inhalten die im eigenen Land gesperrt sind
- Tracking durch ISP verhindern

## Idee & Ziele
Die Idee bei mir zuhause einen eigenen VPN zu hosten ist daraus entstanden dass wir uns überlegt haben wie wir gleichzeitig an verschiedenen Aufträgen arbeiten können. Der einfachste weg dies zu machen wäre natürlich stets einen eigenen Raspi mitzunehmen, allerdings darf dieser dann nicht vergessen gehen, sonst steht man ganz schön blöd da, und am morgen immer noch den doppelten Verkabelungsaufwand zu haben, eine IP aus dem Schulnetz zu bekommen etc. sind alles zusätzliche aufwände die wir uns wenn möglich sparen wollten. Es war also naheliegend einen VPN aufzusetzen, damit dies auch messbar wird, und später eventuell beurteilt werden kann sind hier noch einige Ziele definiert:

- VPN ist gemäss [Anleitung](https://notthebe.ee/raspi.html) installiert und konfiguriert
- Der Zugriff ins Heimnetzwerk ist sowohl vom Desktop PC als auch vom Smartphone aus möglich
- Internet ist durch VPN erreichbar, z.B. https://www.tbz.ch
- Es kann auf Interne Dienste zugegriffen werden, z.B. Router Dashboard oder [RPI-Monitor](https://gitlab.com/m9065/doku/-/blob/main/Arbeitsauftr%C3%A4ge/04_RPI-Monitor.md)
- Hintergründe zur Thematik sowie unser Vorgehen ist dokumentiert, soll jederzeit reproduzierbar sein

## Anleitung
### DynDNS
Bevor wir mit der tatsächlichen installation von [Wireguard](https://www.wireguard.com/) beginnen können, müssen noch einige vorbereitungen getroffen werden.

Als erstes setzen wir dynamisches DNS auf, das ist notwendig weil sich die externe IP, je nach lust und laune vom ISP immer wieder ändern kann, wir werden also einen Hostnamen einrichten der immer in unser Netzwerk zeigt, unabhängig von IP änderungen. Dazu verwenden wir [noip](https://my.noip.com/dynamic-dns) DynDNS kann aber genauso gut über andere Anbieter eingerichtet werden.

Auf der Seite angekommen kann man unter `Dynamisches DNS -> No-IP Hostnames` auf `Hostnamen erstellen` klicken, und alle seine angaben eintragen. Wir wollen einen A Record erstellen, die Domain kann selbst gewählt werden, und als IPv4 sollte man sein externe IP angeben. Diese kann z.B. [hier](https://www.ipchicken.com/) nachgeschaut werden

![DynDNS](img/VPN-Hostname.PNG)

Hat man alles richtig gemacht sieht man in der übersicht einen aktiven Hostnamen:

![Hostname_aktiv](img/VPN-HostCheck.PNG)

### Einrichtung ddclient
Nachdem dies geklappt hat beginnen wir mit der installation von ddclient:
```shell
sudo apt update
sudo apt install ddclient
```

Jetzt können wir ddclient sagen welche adresse er aktuell halten soll, dazu bearbeiten wir folgende configdatei

```shell
sudo nano /etc/ddclient.conf
```

Diese Datei ist standartmässig mit ganz viel Inhalt gefüllt den wir gar nicht brauchen, daher können wir das File komplett leeren, und unseren eigenen Inhalt einfügen:

```
# Configuration file for ddclient generated by debconf
#
# /etc/ddclient.conf
daemon=5m
timeout=10
syslog=no # log update msgs to syslog
#mail=root # mail all msgs to root
#mail-failure=root # mail failed update msgs to root
pid=/var/run/ddclient.pid # record PID in file.
ssl=yes # use ssl-support. Works with
# ssl-library

use=if, if=eth0
protocol=noip
login=YOUR NOIP LOGIN
password=YOUR NOIP PASSWORD
your.noip.domain
```
Jetzt können wir mit **Ctrl+O** speichern und den Editor mit **Ctrl+X** schliessen. 

Eine andere Datei die wir bearbeiten müssen ist `/etc/default/ddclient`. Dort sollte alles auf `false` gesetzt werden, ausser die option `run_daemon` welche `true` ist.

Damit das alles übernommen wird starten wir ddclient mit `sudo systemctl restart ddclient` neu, und enabeln den Dienst via `sudo systemctl enable ddclient` damit er jedesmal automatisch gestartet wird wenn man den Raspi rebootet. 

### Port Forwarding
Im Adminpanel vom Router muss jetzt noch Port Forwarding eingerichtet werden. In meinem Fall dient eine Fritzbox als Router, das Webgui ist über die IP oder über [fritz.box](https://fritz.box) erreichbar.
Dort unter `Internet -> Freigaben` eine neue Portfreigabe mit folgenden angaben einrichten:
- **Gerät:** hostname oder IP vom Raspberry Pi
- **Protokoll:** UDP
- **Port range:** 51820-51820
- **Ausgehender Port:** 51820
- **Internetzugang erlauben:** ja

![portfwd](img/VPN-PortForward.PNG)

### Wireguard installieren
```shell
wget https://git.io/wireguard -O wireguard-install.sh && sudo bash wireguard-install.sh
```

Das installationsscript wird einem nach einigen dingen wie dem VPN Hostnamen und dem DNS Server fragen die man verwenden möchte, hier geben wir den zu beginn erstellten Hostnamen und irgendeinen DNS Server, z.B. `8.8.8.8` an.

Ist das Script erfolgreich durchgelaufen erhält man zum einen ein config file und einen QR Code welcher für die Verbindung mit einem Smartphone nützlich sein kann.

![success](img/VPN-Success.PNG)

### Zum VPN verbinden (Windows)
Hierfür muss man sich den [Wireguard](https://www.wireguard.com/install/) client installieren, und anschliessend auf `Tunnel hinzufügen` klicken. Jetzt wählt man die Konfigurationsdatei aus, und wenn alles geklappt hat ist ein Tunnel zu erkennen.

![Tunnel](img/VPN-WireguardDesktop.PNG)

### Zum VPN verbinden (Android)
Im Playstore kann man sich die Wireguard app herunterladen, unten rechts aufs Plus klicken, und den QR Code scannen. Alles weitere wird automatisch erkannt, und man kann die VPN verbindung per Knopfdruck ein / ausschalten.

![TunnelHandy](img/VPN-WireguardAndroid.PNG)

## Probleme
Da die Anleitung welche wir genutzt haben sehr detailliert und gut nachvollziebar war, gab es keine grösseren Probleme und der VPN funktioniert einwandfrei :-)
Das einzige was am anfang etwas tricky war, war zu verstehen was DynDNS ist, und wozu es hier benötigt wird, ansonsten sind wir sehr zufrieden und konnten alle unsere Ziele innert **ca. 4 Stunden erreichen.**

## Quellen
- https://www.youtube.com/watch?v=rtUl7BfCNMY
- https://notthebe.ee/raspi.html
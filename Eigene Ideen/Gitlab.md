<!-- Das ist ein Kommentar -->
# Dokumentation Gitlab

 - Datum: 03.04.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist Gitlab?
[Gitlab](https://about.gitlab.com/) ist neben konkurrenten wie [Github](https://github.com/) oder [Bitbucket](https://bitbucket.org/), wohl eines der bekanntesten Versionsverwaltungssysteme. Allgemein kann man mit ihnen sehr vieles anstellen, einer der meistgenutzten use-cases ist vermutlich das gemeinsame entwickeln von Software, es können aber auch Dokumentationen wie diese, oder auf bedarf Backups von gewissen Dateien, über z.B. Gitlab zugünglich gemacht werden. Das geniale daran ist dass mehrere Personen am genau gleichen Dokument arbeiten können, ohne dass es konflikte gibt, die änderungen werden wieder schön zusammengemerged. Auch bei grösseren Applikationen könnte man für Feature x, z.B. einen eigene Branch erstellen, dieses Feature entwickeln, und dann wieder in den Master branch mergen, um das Feature der App hinzuzufügen. Die konkreten Umsetzungen dieser Versionsverwaltungstools unterscheiden sich lediglich in Details / Ökosystemen, allerdings ist die grundlegende Idee dahinter immer dieselbe. Jedem der schonmal mit git gearbeitet hat sollte bewusst sein wie der Workflow in etwa funktioniert(`git add, commit, push...`), wir möchten aber trotzdem nochmals ganz kurz darauf eingehen.

### Workflow

![Workflow](img/Gitlab_Workflow.PNG)

Wenn mit git gearbeitet wird, unterscheidet man grundsätzlich zwischen 3, beziehungsweise 4 "Stufen" in denen sich Dateien welche in die Versionierung eingebunden sind, befinden können. Diese lassen sich wiederum in zwei Kategorien aufteilen, einmal Lokal und Remote. Lokal hat man den eigenen Workspace, die staging area, und das Lokale Repository, während remote, auf dem Bild rechts der fetten Linie, noch das Remote Repository, auf irgend einem Server gespeichert ist. 
Wenn man jetzt z.B. ein neues Projekt angefangen hat, und den Source Code oder sonstiges dazu unter Versionskontrolle stellen möchte, kann man mit `git init` ein neues lokales Repository erstellen. Alle änderungen die jetzt gemacht werden passieren auf dem Workspace, der niedrigsten Stufe, wenn man so will. Sobald man mit diesen zufrieden ist kann man sie mit `git add <path>` zur staging area hinzufügen, alles was sich in dieser befindet wird in den nächsten commit reingenommen. Diese werden mit `git commit -m "sinnvolle commitmessage"` ausgeführt, und übertragen alle änderungen ins Lokale Repository. Es dient als eine art Zentraler Aufbewahrungsort, und sobald wichtige Dateien mal dort sind, kann man sich etwas entspannen, da aus dem lokalen repo auch ältere Versionen wiederhergestellt werden können, für den fall dass man mal etwas kaputt gemacht hat. Um seine Files dann nicht nur vor Menschlichem versagen, sondern auch vor Hardwareschäden schützen zu können gibt es Remote Repositories. Diese sind im prinzip eine exakte Kopie des Lokalen Repos, nur halt nicht lokal, sondern physisch an einem anderen Ort. Um die beiden miteinander zu synchronisieren gibt es die Befehle `git push` beziehungsweise `git fetch` oder besser gleich `git pull`.

## Idee & Ziele
Da wir die Thematik rund um git oder allgmein version control recht spannend finden, haben wir uns gefragt ob es wohl möglich wäre eine eigene Instanz einer solchen Anwendung zu hosten, um sich so unabhängig von deren Infrastruktur zu machen. Im Internet fanden wir heraus dass dies mit Gitlab recht leicht umsetzbar ist, und da wir beide sowieso schon vereinzelt damit gearbeitet haben, entschieden wir uns auch gleich für diese Lösung. Erst einmal wollen unsere eigene Gitlab installation haben, auf der sich ein Remote Repository befindet, später werden wir versuchen in einem zweiten, optionalen Schritt eine CICD Pipeline zu bauen. Zu testzwecken könnte man davon ausgehen dass eine website entwickelt werden soll, mit der Pipeline wollen wir dann erreichen dass sich bei jedem commit mit dem z.B. etwas im HTML geändert wird auch gleich von alleine die Website anpasst. Das klingt jetzt vielleicht noch etwas kompliziert, wir werden später nochmals genauer darauf eingehen, hier ersteinmal eine kleine Checkliste mit den dingen die wir effektiv zu erledigen haben:
- [ ] Gitlab ist gemäss Anleitung auf dem Raspberry Pi installiert und konfiguriert
- [ ] Mindestens 2 Benutzer wurden eingerichtet
- [ ] Repository wurde erstellt
- [ ] Beide Benutzer können, von verschiedenen Geräten aus, mit diesem Repository arbeiten
- [ ] CICD Pipeline wurde eingerichtet, und aktualisiert Pages Seite bei commit (*Optional*)
- [ ] Sämtliche Schritte wurden so dokumentiert dass alles gut verständlich & jederzeit nachbaubar ist


## Anleitung
### Vorbereitung
Bevor man mit der eigentlichen installation starten kann müssen noch ein paar dependencies heruntergeladen werden, gitlab funktioniert ohne diese nicht.
```shell
sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
```
Während der installation von postfix taucht ein kleines Konfigurationsfenster auf. Dort wählt man 'Internet Site' aus, und gibt den externen DNS als 'mail name' an. Wie dieser genau lautet spielt ersteinmal keine rolle, wichtig ist nur dass er mit der später definierten Externen URL deckungsgleich ist. Optional kann natürlich auch ein anderes E-mail Benachrichtungsprogramm als Postfix genutzt werden, diesen Schritt dann einfach überspringen.
```shell
sudo apt install -y postfix
```

Als nächstes muss mit folgendem Befehl noch das Gitlab Paket Repository hinzugefügt werden
```shell
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo os=raspbian dist=buster bash
```

Im letzten Schritt vor der tatsächlichen installation sollte man den Domainname den man verwenden möchte um später auf die Gitlab Seite zuzugreifen im DNS Server eintragen. Wir werden hierfür [Pihole](https://gitlab.com/m9065/doku/-/blob/main/Eigene%20Ideen/Pi-hole.md) nutzen, alternativ kann aber auch die Hosts Datei bearbeitet werden, welche im Falle von Windows unter `C:\Windows\System32\drivers\etc\hosts` liegt, um mit dem Hostnamen auf die IP unseres Servers zu zeigen. Diese sollte natürlich auch wieder statisch gesetzt werden, wie das geht werden wir allerdings nicht mehr erklären, genaueres Dazu findet man in unserer Pihole doku.
Ein Eintrag im Hosts File könnte, in unserem Fall, z.B. so aussehen:

![hosts](img/Gitlab_hosts.PNG)

### Installation Gitlab
Ist man mit den Vorbereitungen durch steht der installation nichts mehr im wege, sie wird mit diesem Befehl gestartet, und dauert recht lange. Unter External_URL spezifiziert man, wie zuvor erwähnt, auch gleich schon den Namen über den Gitlab erreichbar ist, erneut darauf achten dass er sich nicht von dem im hosts file eingetragenen unterscheidet. 
```shell
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
```

Wenn alles ohne Fehler durchgelaufen ist kann man seinen Browser öffnen und auf die Seite `https://gitlab.example.com` navigieren, wo man von einem Login screen begrüsst wird.
Der standard username ist `root` und das Passwort welches dazu passt ist für **24 Stunden** in `/etc/gitlab/initial_root_password` gespeichert, nachher läuft es ab, daher würden wir dringend dazu raten das Passwort gleich als erstes abzuändern, dazu aber mehr im nächsten Kapitel.

### Login & Konfigurationen
#### Userspezifisch <!-- Zerst config uf user, nachher uf ganzes system -->
##### Passwort ändern
Hat man sich einmal mit dem Initialpasswort angemeldet kann oben rechts auf seinen Account und **Edit Profile** klicken.

![oberechts](img/Gitlab_oberechts.PNG)

Links unter dem Punkt **Password** kann es dann auch gleich schon angepasst werden, somit ist der 24 Stunden Timer entschärft, es gibt nichts mehr was unter Zeitdruck gemacht werden müsste, und wir können den Rest der installation ganz in ruhe durchführen.

##### SSH Key
Damit man später nicht nur via http, sonder auch über ssh repositories klonen kann, und überhaupt erst eine sichere Verbindung zwischen dem eigenen Computer und Gitlab aufbauen kann wird es empfohlen einen SSH key mit dem Userkonto zu verknüpfen. Im Gitlab geht man dazu wieder auf die User Settings (wie bereits auf dem vorherigen Bild gezeigt), und dann auf **SSH Keys**. Öffnet man das Menü zum ersten mal wird der Screen in etwa so aussehen:

![sshempty](img/Gitlab_sshempty.PNG)

In das Leere Feld was mit **Key** beschriftet ist, muss man später seinen Public Key einfügen. Um ein Schlüsselpaar zu generieren kann man, unter Windows, die Eingabeaufforderung öffnen, und den Befehl `ssh-keygen` absetzen. Hat man alles richtig gemacht werden 2 Dateien generiert, einmal Public und Private Key, diese liegen standartmässig unter `C:\Users\mmuster`. Jezt öffnet man die Datei mit der Endung `.pub` im Editor seiner Wahl, und kopiert sich den Inhalt raus, um ihn gleich anschliessend ins dafür vorgesehene Gitlab Fenster zu Pasten und den **Add Key** button zu drücken.
Im Optimalfall sieht das Menü jetzt etwa so aus:

![sshdone](img/Gitlab_sshdone.PNG)

#### Systembezogen
Alle Konfigurationen die nicht nur auf einen einzelnen User, sondern auf das komplette System greifen werden im **Adminbereich** festgelegt.

![Adminbereich](img/Gitlab_admin.PNG)

Dort begrüsst einem direkt ein kleines Dashboard, wo man auf einen blick erkennen kann wieviele Benutzer, Projekte etc. aktiv sind, aber auch welche Versionen die einzelnen komponenten haben.

![Dashbaord](img/Gitlab_dashboard.PNG)
##### Allgemein
<!-- Security, nöd selber account registriere, pw min 8 zeiche... -->
Die Allgemeinen Einstellungen machen wir, wie der Name schon sagt, unter **Settings** beziehungsweise **General**. dort gehen wir zum Punkt **Sign-up restrictions** und entfernen den Haken bei "Sign-up enabled". Da die Gitlab instanz in unserem Falle sowieso bei mir zuhause läuft, und nicht öffentlich zugänglich ist, ist es für uns spezifisch nicht ganz so wichtig was hier ausgewählt wird. Sobald man sie aber im Internet zugänglich machen würde, könnte sich jeder der die Seite findet ein eigenes Konto eröffnen ganz normal damit Arbeiten, wir haben aber nicht unendlich Ressourcen zur Verfügung wesshalb wir das in der Regel möglichst verhindern wollen.
Weiter unten im selben Menüpunkt könnte man dann z.B. noch die minimale Passwortlänge angeben, an die sich Benutzer halten müssen, standartmässig sind dies 8 Zeichen, wir lassen es daher gleich so. 

![signuprestriction](img/Gitlab_signuprest.PNG)

Allgemein lässt sich hier enorm viel einstellen, fast schon zu viel als dass wir auf alles eingehen könnten, falls man gerade dabei ist sich ein eigenes Gitlab aufzubauen, nimmt man sich am besten kurz Zeit alles in ruhe durchzuschauen, und Individuell zu entscheiden welche Optionen für einem Sinn machen, oder eher weniger.

##### Gruppen & Benutzer erstellen
<!-- eif mal für mich und jb mache -->
Gruppen und Benutzer können ebenfalls in der Adminansicht unter **Overview** und **Users** oder **Groups** erstellt werden. Wir beginnen mit den Benutzern, dazu gehen wir auf den besagten Menüpunkt und klicken auf den Button **New User**.
Diesem geben wir dann einen Namen, Benutzernamen und eine Emailadresse, wählen aus dass er Admin rechte haben soll, und Validieren den Account auch gleich. Zu unterst gibt es dann nochmals einen Knopf über den der User effektiv angelegt werden kann.

![newuser](img/Gitlab_newuser.PNG)

Denselben Prozess füren wir jetzt nochmals durch, um einen zweiten Benutzer erstellen zu können. In einem zweiten Schritt bearbeiten wir den Benutzer und setzten gleich das Passwort, somit müssen wir es nicht noch mühsam per mail bestätigen. 

![passwd](img/Gitlab_passwd.PNG)

Um zu testen ob die Benutzer auch tatsächlich geschluckt wurden, melden wir uns Oben rechts, über **Sign Out** ab, und loggen uns mit dem neuen Account ein. Wie auf dem Bild zu sehen bin ich nun mit dem user `tschefer` angemeldet, es hat somit funktioniert.

![tschefer](img/Gitlab_tschefer.PNG)

Da wir nun zwei Benutzer haben können wir noch eine Gruppe erstellen, und unsere Benutzer zu dieser hinzufügen. Dies machen wir wieder über **Overview** und dann **Groups**. Dort klickt man auf Add new Group und füllt alles nach seinen Bedürfnissen aus, das endergebnis sieht etwa so aus:

![newgroup](img/Gitlab_newgroup.PNG)

Wurde die Gruppe erstellt erscheint eine kleine Übersicht:

![overview](img/Gitlab_grpOverview.PNG)

Wenn man hier etwas genauer hinsieht fällt einem auf dass auch gleich Benutzer hinzugefügt werden können, was wir natürlich machen.

![adduser](img/Gitlab_addUser.PNG)

An dieser Stelle wären wir eigentlich auch schon fertig mit dem einrichten der Benutzer & Gruppen, somit können wir zum nächsten Punkt übergehen.

##### Customization
Wenn man seine Gitlab instanz noch etwas personalisieren möchte, und sich nicht mit den Standartmässigen Logos zufrieden gibt, kann man diese in der Admin Area unter **Settings** und **Appearance** anpassen. 
Als erstes kann man hier zwei Logos für die Navbar (oben links), und das Favicon (icon im Browsertab) festlegen.

![NavFav](img/Gitlab_NavFav.PNG)

Da wir aus dem Vergangenen Modul noch ein Logo von der ICT-Consulting AG haben, verwenden wir zu testzwecken gleich dieses. Das ergebnis sieht dann so aus

![NavFavReal](img/Gitlab_NavFavReal.PNG)

Als nächstes kann man einen Header beziehungsweise Footer festlegen, und diesen sogar noch mit eingenen Farben versehen.

![HeadFoot](img/Gitlab_HeaderFooter.PNG)
![HeadFootReal](img/Gitlab_HeaderFooterReal.PNG)

Das letzte was wir dann noch einstellen hat mit dem Login zu tun, wir legen fest dass ein Spezieller Titel sowie beschreibung verwendet werden soll, für die beschreibung kann übrigens auch [Markdown](https://de.wikipedia.org/wiki/Markdown) Syntax genutzt werden.

![signupdesign](img/Gitlab_signup.PNG)

Das fertige Resultat sieht bei uns jetzt etwa so aus:

![singupfinished](img/Gitlab_signupfinish.PNG)


#### Monitoring
Für den Fall dass man sein System noch etwas genauer überwachen möchte, gibt es in der Admin Area den Punkt Monitoring.

![Monitoring](img/Gitlab_Monitoring.PNG)

Dort sieht man zuerst einmal einige Systeminfos wie Festplatten / RAM auslastung, oder auch die Uptime. Will man es dann noch genauer wissen, kann man auf Background Jobs klicken, und bekommt schön Grafisch dargestellt wie es um Jobs steht.

![Monitoring2](img/Gitlab_Monitoring2.PNG)

### Erstes Repository
<!-- https://confluence.atlassian.com/fishkb/unable-to-clone-git-repository-due-to-self-signed-certificate-376838977.html-->
Damit wir auch tatsächlich mit Gitlab arbeiten können brauchen wir zuerst einmal ein Remote Repository in dass Files gepusht werden können. Dazu klicken wir oben, eher rechts, auf das Plus Symbol und **New project / repository**. 

![newrepo](img/Gitlab_newproject.PNG)

Jetzt gibt es drei auswahlmöglichkeiten. Wir können entweder ein komplett Leeres Projekt erstellen, von einem Template starten, oder ein bereits bestehendes Repository importieren. In unserem Fall gehen wir mit der ersten option, also mit dem komplett leeren Repository, da wir es nur zu testzwecken brauchen werden.
Dann können wir diesem noch einen Namen geben, und einen Scope festlegen, ehe es tatsächlich erstellt wird.

![createproject](img/Gitlab_createProject.PNG)

Wenn das Repo erfolgreich erstellt, und mit Readme initialisiert wurde, erwartet einem dieser Anblick:

![repogitlab](img/Gitlab_repoonline.PNG)

Um Dateien in dieses zu Pushen gibt es zwei möglichkeiten:
1. Über das Plus oben auf **Upload Files* klicken, und Dateien auswählen
2. Repository klonen, und Files manuell pushen

Da die erste Option mehr oder weniger Selbsterklärend ist gehen wir nicht weiter darauf ein, sondern konzentrieren uns auf zweiteres. Wenn wir das Repo klonen, und sozusagen in Windows "einbinden" wollen, müssen wir auf den entsprechenden Button drücken, und eine der Beiden Varianten wählen. SSH geht nur sofern wir den SSH key, wie zuvor gezeigt, eingerichtet haben, mit http sollte man auf der sicheren seite sein, da dies grundsätzlich immer funktioniert.

![clone](img/Gitlab_clone.PNG)

Sobald wir die URL kopiert haben suchen wir uns auf dem eigenen Rechner einen Ordner in den wir Klonen möchten, rechtsklicken diesen, und wählen die Option Git Bash hier. Das setzt voraus, dass wir [git](https://git-scm.com/) installiert haben, wer das noch nicht hat, sollte es sich also kurz herunterladen.

![bashhere](img/Gitlab_gitbashhere.PNG)

In der Konsole können wir den Befehl `git clone <REPO-URL>` eingeben, und das Repository sollte problemlos geklont werden können. Wenn dem so ist, sehen wir einen Ordner der sich mit exakt den selben Files gefüllt hat, stand jetzt ist er, bis auf das Readme, und einen .git ordner, der mit Git configs gefüllt ist, noch leer, wir können aber darin arbeiten, und dateien später ins Remoterepository pushen.

![clonefolder](img/Gitlab_clonefolder.PNG)

Wenn wir z.B. mal davon ausgehen dass wir in dem Repo Dokumentationen speichern wollen, können wir eine neue Datei, mit dem Namen `Testdoku.md` erstellen, und mit Inhalt befüllen.

![editedfolder](img/Gitlab_editedfolder.PNG)

Innerhalb der Git Bash Konsole kann man jetzt den Befehl `git status` eingeben, um zu sehen was sich alles verändert hat, was für änderungen in den nächsten commit mit aufgenommen werden könnten. Möchte man nach getaner arbeit schliesslich alles pushen sind folgende Befehle, im Root Directory abzusetzen:

```bash
git add .
git commit -m "commitmessage"
git push
```
Wenn das alles erfolgreich durchlaufen konnte, und keine Konflikte entstanden sind, hat man seine gepushten Files jetzt einmal Lokal und einmal auf Remote dem Gitlab Server, man könnte sie also problemlos wieder herstellen, sollte lokal irgendetwas passieren.
Für uns ist dies auch gleichzeitig der Beweis das alles funktioniert hat.

![filepushed](img/Gitlab_filepushed.PNG)

## CI/CD
### Was ist das?
Die Abkürzung CI/CD steht für **C**ontinous **I**ntegration / **C**ontinous **D**eployment, und beschreibt eine der gängigsten Devops praktiken. Mithilfe von sogenannten CI/CD Pipelines können gewisse abläufe automatisiert werden, so dass man als Entwickler z.B. stets eine Lauffähige version seines Programmes hat. So eine Pipeline würde z.B. getriggert werden sobald man irgendwelchen Code in sein Repositoriy pusht. Dann würde zuerst einmal versucht werden diesen Code zu kompilieren, und einen lauffähigen Build zu erstellen. Schägt dies fehl bekommt man als entwickler vielleicht eine Benachrichtigung, und weiss was man noch anpassen muss. Im nächsten Schritt würden vielleicht gewisse Testcases die schon im vorhinein definiert wurden, ausgeführt werden, und sobald diese bestanden wurden könnte man seinen Code direkt schon deployen so dass er mehr oder weniger produktiv eingesetzt werden kann.

![cicd](img/Gitlab_cicd.PNG)

Gitlab bietet einem die möglichkeit mit solchen Pipelines herumzuspielen, wir wollen diese funktion nutzen, um eine kleine Testwebsite zu bauen, die mittels Gitlab [Pages](https://docs.gitlab.com/ee/user/project/pages/) dargestellt wird, und gleich von alleine alle änderungen übernimmt die wir Pushen.

### Setup
Bevor wir mit der konfiguration der eigentlichen Pipeline starten können, müssen wir noch einen [Runner](https://docs.gitlab.com/runner/) einrichten. Dies ist eine separate Softwarekomponente die später alle Pipelines ausführt. Aus Performance und Sicherheitsgründen empfiehlt es sich eigentlich diesen Runner auf einem separaten Gerät zu installieren, da wir aber nur einen Pi zur verfügung haben, können wir das nicht machen.

#### Runner installieren
Die Installation machen wir nach dieser [Anleitung](https://medium.com/devops-with-valentine/use-your-raspberry-pi-to-run-gitlab-ci-jobs-8cc29fa49dbe)

Zuerst muss Docker installiert werden, damit der Runner später in der Lage ist seine Pipelines in Containern auszuführen. Dzu laden- wir uns mit folgendem Befehl das Docker installationsscript herunter, und führen es aus:
```bash
curl -sSL https://get.docker.com | sh
```

Nachher fügen wir den Benutzer pi zur docker gruppe hinzu, und verifizieren dass er mitglied von dieser ist.
```bash
sudo usermod -aG docker pi
groups pi
```

Damit Docker gleich jedesmal automatisch startet aktivieren wir den Dienst und rebooten den Pi:
```bash
sudo systemctl enable docker
sudo reboot
```

Das Docker jetz wirklick erfolgreich installiert ist können wir mit `docker run hello-world` prüfen, im optimalfall bekommen wir etwa so einen Output:

![dockerhw](img/Gitlab_dockerhelloworld.PNG)

Jetzt können wir auch schon mit der tatsächlichen installation des Runners beginnen, je nach Systemarchitektur ist hier eine andere Version Notwendig, wir überprüfen die unsere wie folgt:
```bash
dpkg --print-architecture
```
Da wir mit dem Raspberry Pi arbeiten, bekommen wir `armhf` als output und installieren somit die entsprechende Version:
```bash
curl -O "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_armhf.deb"
sudo dpkg -i gitlab-runner_armhf.deb
```

Der Runner wurde jetzt erfolgreich Installiert, und läuft bereits, was wir im Übrigen auch mit folgendem Befehl überprüfen können:
```bash
sudo gitlab-runner status
```

#### Runner registrieren
Um den Runner dann auch zu Registrieren, ihm also zu sagen von welcher Gitlab Instanz er seine Jobs entgegennehmen soll, drücken wir im Gitlab Webgui **Ctrl + G** und wählen unsere zuvor erstellte Gruppe aus. Dann gehen wir links auf **Settings** und **CICD** und klappen die Kategorie **Runners** auf. Dort **deaktivieren** wir die Option **Enable Shared Runners** und kehren auch schonwieder auf den Pi zurück, wo wir folgenden Befehl eingeben, worauf sich ein Menü öffnet von dem aus man den Runner hinzufügen kann.
```bash
sudo gitlab-runner register
```
Hier sind vor allem die ersten zwei Punkte relevant, hier muss nämlich die URL und der Access Token konfiguriert werden, welche man beide seinem Webgui entnehmen kann.

![token](img/Gitlab_token.PNG)

Den rest bestätigt man mit enter, und eigentlich sollte er jetzt erfolgreich Registriert worden sein, das ist bei uns aber nicht der Fall, sondern wir bekommen eine Fehlermeldung:

![error1](img/Gitlab_error1.PNG)

#### Troubleshooting
Da in der Fehlermeldung der Port 53 auftaucht, sind wir auf die Idee gekommen dass etwas mit dem DNS nicht stimmen kann, was durchaus sinn macht, da der Raspi natürlich versucht https://gitlab.pi.local aufzulösen, aber die Domain im DNS Server den der Pi verwendet nicht bekannt ist. Um dies zu umgehen haben wir mit diesem Befehl einen eintrag in die Hosts Datei gemacht:
```bash
sudo nano /etc/hosts
192.168.178.10  gitlab.pi.local
```

Nach einem neustart und dem erneuten versuch den Runner zu Registrieren passierte schonmal etwas anderes, und zwar tauchte plötzlich diese Fehlermeldung auf:

![error2](img/Gitlab_error2.PNG)

Sie lässt uns vermuten dass irgendwas mit dem SSL Zertifikat nicht in Ordnung ist, vermutlich hat Gitlab Probleme damit dass es Selfsigned ist. Im Internet haben wir folgenden [Beitrag](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28841) gefunden, von jemandem der genau dasselbe Problem wie wir hat, nachdem wir seine lösung durchgeführt haben kam aber immernoch dieselbe Meldung.

Auch sonst haben wir einiges probiert, aber ohne erfolg, die einzige möglichkeit die wir aktuell sehen, ist für den Optionalen Teil, also das ganze CI/CD zeugs, auf die offizielle Gitlab.com Seite zu wechseln, und das ganze dort durchzuführen.
Natürlich wäre es cooler gewesen dies auf unserer eigenen Instanz laufenzulassen, aber mit unserem aktuellen Wissen schaffen wir das nicht, und vom konzept her funktioniert es ja trotzdem genau gleich, hoffentlich gibt es dafür also nicht allzuviel abzug :)

#### Setup auf Gitlab.com
Wir melden uns also auf Gitlab.com an und erstellen ein neues Projekt. Diesmal sagen wir dass wir von einem Template aus starten wollen, und wählen dann `Pages/Plain HTML`.

![demo](img/Gitlab_demoprojekt.PNG)

Dann müssen wir kurz warten bis alles erstellt wurde, und werden daraufhin auch schon von foglender Struktur begrüsst:

![struktur](img/Gitlab_demostruktur.PNG)

Wir haben bereits einen Ordner mit dem Namen Public in dem sich eine Sample Website befindet und ein File namens `.gitlab-ci.yml`, in dem definiert ist wie die Pipeline genau aufgebaut sein soll.

![gitlab-ci.yml](img/Gitlab_cicdyml.PNG)

In diesem Falle ist es sehr simpel, und es passiert sogut wie nichts, es wird lediglich definiert dass die Artifakte welche später auf der Pages seite dargestellt werden im Public Ordner liegen, man könnte es allerdings auch noch wesentlich komplexer machen, wenn man z.B. eine Tatsächliche Applikation hätte, und nicht blos HTML.

Wenn links unter **Settings** auf **Pages** drücken sehen wir unter welcher URL dass die [Demoseite](https://tschefer.gitlab.io/democicd) erreichbar ist, und wir können sie gleich öffnen.
Das Grundsätzliche Setup war an sich also ganz einfach, jetzt muss man nur noch testen ob gepushte änderungen auch tatsächlich übernommen werden.

### Test
Fürs Testing klonen wir das Repository als erstes. Dann öffnen wir das `index.html` File und ergänzen eine neue Zeile.

![newline](img/Gitlab_htmlchanges.PNG)

Jetzt pushen wir die änderungen und schauen was passiert:

Nachdem die Seite im Browser neugeladen wurde, können wir oben erkennen dass die Pipeline getriggert wurde.
![pipelinerunning](Gitlab_pipelinetriggered.PNG)

Wenn wir dann draufklicken sehen wir noch genau was im Hintergrund mit Docker gemacht wird, und bekommen bescheid sobald alles erfolgreich war.
![pipelinefinish](Gitlab_pipelinefinished.PNG)

Jetzt bleibt nurnoch die Seite zu überprüfen, und tada, alle änderungen wurden übernommen.
![site](Gitlab_site.PNG)

## Probleme
<!-- Package repository nöd ohni os parameter und so am schluss, https selfsigned option mitgeh, runner -->
Wir hatten verschiedene Probleme bei der Installation wovon zum Glück die meisten Überwunden werden konnten.

Zuerst einmal als wir das Paketrepository für die installation von Gitlab hinzufügen wollten. Der Befehl in der Anleitung von Gitlab lautet nämlich
```bash
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
```
funktioniert, auf dem Raspberry Pi aber noch nicht direkt so, man muss die parameter `os=raspbian` und `dist=buster` mitgeben, ansonsten erscheint eine Fehlermeldung, der Fertige befehl sieht dann also irgendwie so aus:
```bash
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo os=raspbian dist=buster bash
```

Das zweite machte sich auch schon bald bemerkbar, nämlich war das als wir das erste selbst erstellte Repository über https klonen wollten. Hier meckerte git bereits dass unsere Instanz, wegen des Self Signed Zertifikates nicht sicher ist, und dass es somit nicht möglich ist über https zu klonen. Dafür haben wir aber recht schnell einen Workaround gefunden, mit dem Befehl der gleich zu sehen ist, kann man dieses Sicherheitsfeature nämlich entweder direkt deaktivieren, oder für den Moment umgehen:

1. Für den Moment
```bash
git -c http.sslVerify=false clone <repository-name>
```

2. Systemweit
```bash
git config --global http.sslVerify false
```

Das letzte problem mit den Runnern was leider nicht komplett gelöst werden konnte wurde schon weiter oben beschrieben.
Ich würde sagen dass das bis jetzt mit abstand der Anspruchsvollste auftrag von allen war, wenn ich den Aufwand ungefähr in Stunden abschätzen müsste würde ich vielleicht auf **8 Stunden** tippen.

## Quellen
- https://about.gitlab.com/install/#raspberry-pi-os
- https://medium.com/devops-with-valentine/use-your-raspberry-pi-to-run-gitlab-ci-jobs-8cc29fa49dbe
- https://confluence.atlassian.com/fishkb/unable-to-clone-git-repository-due-to-self-signed-certificate-376838977.html
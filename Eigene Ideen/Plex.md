<!-- Das ist ein Kommentar -->
# Dokumentation Plex Media Server

 - Datum: 24.04.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist Plex?
Plex ist ein client server Mediaplayer der es einem als Benutzer ermöglicht, Sammlungen von Fotos, Video / Audiodateien zu verwalten. Dabei kann man selber Benutzer erstellen, diese auf gewissen Inhalte berechtigen usw., man kann es sich vielleicht ein bisschen wie eine abgewandelte Version des Streamingdienstes Netflix vorstellen, mit dem Unterschied dass nicht nur Serien & Filme Möglich sind, sondern auch Bilder oder Life TV und man das ganze selber, z.B. wie wir auf einem Raspberry Pi hosten kann. Das coole daran ist dass man für die Software nichts bezahlen muss, wenn man mit der Zeit also mal ein paar Filme / Serien gesammelt hat, kann man einfach eine Festplatte mit diesen an den Raspi anschliessen, und nachher von Überall aus darauf zugreifen. Um dies zu ermöglichen gäbe es ein Feature bei dem auf dem Router ein Port freigegeben werden muss, da wir aber sowieso schon einen VPN aufgesetzt haben, wird das für uns nicht mehr nötig sein & wir können direkt über diesen auf die Inhalte zugreifen, sollten wir nicht zuhause sein.

## Idee & Ziele
Heutzutage gibt es mit Netflix, Disney+ und co. bereits soviele verschiedene Aboanbieter dass es ganz schön schmerzhaft fürs Portemonnaie sein kann wenn man sich mehrere Inhalte anschauen möchte, die exklusiv auf unterschiedlichen Plattformen laufen. Die entsprechenden Dateien lassen sich aber relativ leicht übers Internet oder andere Wege organisieren, wenn man sich also mal eine kleine Sammlung zusammengestellt hat, kann man so alles anschauen, ohne sich über Kosten, oder komische Werbung von kostenlosen Streaminganbietern aufregen zu müssen.

Unsere Ziele sind:
- Plex wurde gemäss [Anleitung](https://www.youtube.com/watch?v=Gqxlc4iYrNg) installiert und konfiguriert
- Es können eigene Inhalte (Videos & Audiodateien) wiedergegeben werden
- Es können Inhalte aus der Plex Mediatek wiedergegeben werden (Live TV...)
- Mindestens 2 Benutzer wurden eingerichtet
- 1 Benutzer wird nur auf bestimmte Inhalte berechtigt, hat z.B. keinen Zugriff auf eigene Serien (Optional)
- Sämtliche Schritte wurden so dokumentiert dass sie leicht zu verstehen, und jederzeit nachbaubar sind

## Anleitung
### Raspi
Sobald der Raspi mal mit einer Statischen IP und dem ganzen Zeugs einsatzbereit gemacht wurde, kann die installation beginnen. Zuerst laden wir uns folgendes Paket herunter:
```shell
sudo apt update
sudo apt install apt-transport-https 
```
Es wird zwar mit grosser Wahrscheinlichkeit schon auf dem Pi installiert sein, falls man aber mit der Lite Version Unterwegs ist, ist dieser schritt notwendig.

Dann fügen wir das Plex Repository zu unseren apt Paketquellen hinzu, und aktualisieren diese erneut:
```shell
sudo curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
sudo apt update
```

Jetzt kann der Plex Media Server auch schon installiert werden:
```shell
sudo apt install plexmediaserver
```
Falls wir während der Installation gefragt werden ob wir auf eine andere Version wechseln wollen drücken wir `N` und bestätigen mit `Enter`.

### Webgui
Sobald diese Schritte durchgelaufen sind, kann man auf sein Webgui wechseln, und die URL `http://<IP-Adresse>:32400/web` aufrufen, wo man von folgendem Screen begrüsst wird.

![Firstvisit](img/Plex_registeracc.PNG)

Um sich hier einloggen zu können braucht man zuerst einen Plex account, dieser kann [hier](https://www.plex.tv/) kostenlos erstellt werden.

![createacc](img/Plex_createacc.PNG)

Jetzt geht man wieder zurück auf seine eigene Seite, und Meldet sich über die Email Adresse an. Daraufhin muss man die Seite kurz neuladen, und es erscheint das erste Fenster zur tatasächlichen einrichtung:

![signin](img/Plex_setup1.PNG)

Wenn man dieses Bestätigt kann man seiner Installation einen Namen geben, in unserem Fall 'brombeeri', und auswählen, ob das ganze von Extern zugänglich gemacht werden soll. Da wir sowieso schon einen VPN haben, sind wir nicht mehr auf dieses Feature angewiesen, und entfernen den haken.

![name](img/Plex_setup2.PNG)

Als nächstes könnte man bereits seine Mediathek importieren, wir werden dies aber in einem späteren Schritt machen, und überspringen hier erstmal. 

![import](img/Plex_setup3.PNG)

Jetzt ist die Ersteinrichtung grundsätzlich fertig, die Oberfläche sollte etwa so aussehen wenn man alles richtig gemacht hat:

![Interface](img/Plex_interface.PNG)

Man sieht zuerst einmal alle Serien die man bereits begonnen hat, und aktuell am schauen ist, dann bekommt man einige vorschläge von Plex selber und zuunterst sieht man dann noch welcher Inhalt gerade läuft, man kann ihn über die Üblichen Tasten pausieren, spulen etc.

### Konfiguration Plex
#### Video Hinzufügen
Wenn man links zuunterst auf `Mehr` drückt erscheint zu oberst ein Eintrag mit dem Namen `brombeeri`. Dort kann man auf das Plus drücken um eigene Inhalte hinzuzufügen.

![Plus](img/Plex_addmedia.PNG)

Zuerst muss man sich entscheiden was man hinzufügen möchte. Wir gehen hier auf Serien, geben dem "Ordner" den wir Importieren einen Namen, und wählen die Passende Sprache aus. In dieser werden später auch beschreibungen zum Inhalt, Schauspielern etc. dargestellt. 

![addmedia2](img/Plex_addmedia2.PNG)

Dann braucht man nur noch anzugeben wo auf dem Pi, sich diese inhalte befinden, wenn man sich nicht sicher ist, kann man, über die Konsole, ins entsprechende Unix Verzeichnis Navigieren und den Befehl `pwd` (**P**rint **W**orking **D**irectory) eingeben, um sich den Absoluten Pfad anzeigen zu lassen.

Hat man noch keine Dateien auf dem Raspberry Pi gibt es mehrere Möglichkeiten sie auf ihn zu kopieren. Der einfachste Weg ist es vermutlich die Files von seinem Windows Rechner aus auf einen USB stick zu kopieren und diesen einzustecken, alternativ könnte man aber auch das Programm [Filezilla](https://filezilla-project.org/) nutzen, um die Dateien übers Netzwerk in den gewünschten Ordner zu kopieren. Dieses ist so aufgebaut dass man zuoberst die IP des Pis und den Benutzernamen / Passwort / Port angeben muss, über den man sich verbinden will. Dann hat man zwei Explorer fenster eines mit dem Dateisystem des Host und ein anderes mit dem des Zielrechners. Das coole daran ist dass man drag and drop gearbeitet werden kann, man braucht sich keine Langen scp befehle merken.

![filezilla](img/Plex_filezilla.PNG)

Die Inhalte dann hinzugefügt wurden, sieht man seine Sammlung und alle Shows die darin enthalten sind:

![sammlung](img/Plex_sammlung.PNG)

Man kann auf eine bestimmte serie klicken, und sieht die Details darüber:

![jjk](img/Plex_success.PNG)

#### Musik hinzufügen
Der Prozess um Musik hinzuzufügen ist im Prinzip recht ähnlich. Auch hier geht man wieder übers Plus, wählt aber aus dass man Musik hinzufügen möchte.

![Music](img/Plex_addmusic1.PNG)

![music2](img/Plex_addmusic2.PNG)

Alle songs sieht man dann hier:

![songs](img/Plex_songs.PNG)

#### Benutzer anlegen & berechtigen
Um Benutzer anzulegen klickt man oben rechts auf sein Icon, und dann `Benutzer wechseln...`. Jetzt wird man auf folgende Seite weitergeleitet:

![adduser](img/Plex_adduser.PNG)

Dort wählt man die Option Benutzer hinzufügen, gibt dem User einen Namen und drückt auf weiter.

![lars](img/Plex_lars.PNG)

In einem weiteren Schritt hat man nun die möglichkeit diesem User berechtigungen auf bestimmte Inhalte zu geben. In unserem Beispiel wollen wir dass er zwar Musik hören kann, aber nicht sieht was wir für Serien haben.

![permissions](img/Plex_perm.PNG)

Optional kann man seinem Benutzer noch einen Pin geben, damit sich nicht jeder damit anmelden kann, das geht in den Einstellungen unter **Plex Home & Mediatheken zugriff**

![users](img/Plex_users.PNG)

Beim Einloggen sieht das ganze dann wie folgt aus:

![login](img/Plex_pin.PNG)


## Test
Soweit wurde jetzt alles konfiguriert, und wir können überprüfen ob wir überhaupt zugriff auf unsere Inhalte haben. Dazu klicken wir zuerst auf unsere Anime sammlung und Probieren die erste Folge 'Jujutsu Kaisen' abzuspielen. 
Wie man sieht funktioniert das ganze einwandfrei, auch wenn die Ladezeiten nicht gerade optimal sind. 

![play](img/Plex_play.PNG)

Auch bei der Musik funktioniert das ganze ohne Probleme, da man diese nicht sehen kann müsst ihr es uns einfach glauben.

### Dashboard
<!-- Dashboard wer was macht -->
Für den Administrator gibt es in den **Einstellungen** unter **Status** beziehungsweise **Dashboard** noch ein kleines Dashboard, auf dem man sieht, welcher Benutzer gerade welche Inhalte konsumiert. Für uns ist dies ein weiterer Weg zu beweisen das alles geklappt hat.

![Dashboard](img/Plex_Dashboard.PNG)

## Probleme
Die installation von Plex ist ausnahmsweise mal komplett Reibungslos verlaufen, und uns war kein einziges Hinderniss im Weg. Dementsprechend liess sich das ganze Projekt relativ schnell bewerkstelligen, die Dokumentation brauchte sogar länger als das tatsächliche Realisieren, insgesamt sind wir mit geschätzten **3 Stunden** aufwand durchgekommen.

## Quellen
- https://www.plex.tv/
- https://www.youtube.com/watch?v=Gqxlc4iYrNg
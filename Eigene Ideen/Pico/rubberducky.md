<!-- Das ist ein Kommentar -->
# Dokumentation Rubber Ducky

 - Datum: 01.05.2022
 - Name: Tim Schefer, Justin Barthel

## Was ist der Raspberry Pi Pico?
Der "normale" Raspberry Pi 4 kann im Grunde wie ein normaler Desktop PC genutzt werden. Es lassen sich die verschiedensten Peripheriegeräte an ihn anschliessen, und er ist dazu in der lage SD Karten mit ganzen Linux Betriebssystemen aufzunehmen. Beim [Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) sieht das etwas anders aus, hierbei handelt es sich nämlich um einen Microcontroller, das Gerät soll also einen ganz anderen Usecase erfüllen, und nicht als Computer im klassichen Sinne, sondern zur Steuerung von Elektronik und dergleichen oder zur Signalverarbeitung etc. genutzt werden. Von Sensoren bis zu kleinen LCD Screens lassen sich die verschiedensten Dinge damit kontrollieren, wir werden ihn allerdings in einen [Rubber Ducky](https://de.wikipedia.org/wiki/Rubber_Ducky) verwandeln, und somit gewisse Programmabläufe ausführen lassen, sobald man das gerät in seinen PC einsteckt.

![Pico](img/Pico_digitec.PNG)

## Was ist Rubberducky?
<!-- Badusb allgemeinh HID.. -->
Der Rubberducky ist ein Gerät welches einem USB Stick verdächtig ähnlich aussieht. Anderst als bei einem solchen handelt es sich aber nicht um einen Datenträger, sondern um ein Tool mit dem Tastatureingaben automatisiert werden können. Das ganze funktioniert so dass man im voraus ein kleines Script als Payload vorbereitet, in dem dann definiert wird welche Tastatureingaben später von alleine ausgeführt werden sollen. Sobald man den Rubberducky dann in einen Rechner steckt, werden genau diese anweisungen ausgeführt, und zwar mit einer Geschwindigkeit die man als Mensch niemals erreichen wird.

## Idee & Ziele
<!-- Langsam ideen ausgegangen mit normalem pi, wollten trotzdem noch was machen also pico gekauft -->
Als uns langsam die Ideen für "normale" Raspberry Pi Projekte ausgegangen sind, da wir doch schon relativ viel eigenes gemacht haben, sind wir auf den Raspberry Pi Pico gestossen. Das Gerät kostet gerade mal 10.- und wir waren nach etwas recherche so beigeistert davon dass wir ihn uns sowieso geholt hätten, auch unabhängig von der Schule. Zwar sind wir uns nicht ganz sicher ob Microcontroller, wie der Pico, überhaupt noch in die Modulidentifikation passen, und uns Notentechnisch weiterbringen, allerdings interessieren wir uns, wie gesagt sowieso dafür, und der Hersteller bleibt ja Raspberry Pi also wieso nicht noch schnell eine kleine Doku schreiben.
Mit dem Pico wollen wir uns selber eine art Rubber Ducky basteln, da dieser viel zu teuer ist, und unsere Lösung schlussendlich dasselbe kann. Das Ziel ist es ein [HID](https://de.wikipedia.org/wiki/Human_Interface_Device) zu simmulieren, und dadurch in [Ducky Script](https://docs.hak5.org/usb-rubber-ducky-1/the-ducky-script-language/ducky-script-quick-reference) geschriebene Befehle ausführen zu lassen, um als Scherz z.B. Bluescreens zu erzeugen sobald man den Pico einsteckt. Theoretisch könnten aber auch viel ernsthaftere Attacken ausgeführt werden, man sollte also immer aufpassen was man für USB Geräte mit dem eigenen Rechner verbindet.

Unsere Konkreten Ziele sind:
- Funktionsweise des Raspberry Pi Pico verstehen (unterschiede zum gewöhnlichen Raspi...) 
- Pico wurde mithilfe dieser [Anleitung](https://github.com/dbisu/pico-ducky) zum Rubber Ducky umfunktioniert
- Es können eigene und vorgefertigte Payloads aus dem Internet verwendet werden
- Das Tastaturlayout wurde auf Deutsch Schweiz geändert (optional)
- Sämtliche Schritte wurden so dokumentiert dass sie leicht zu verstehen, und jederzeit nachbaubar sind

## Anleitung
### Circuitpython
Als erstes müssen wir Circuitpython installieren. Dabei handelt es sich um eine spezielle Implementierung der Programmiersprache Python, wir brauchen sie um später Duckyscript ausführen zu können. Der ganze ablauf ist nämlich grundsätzlich so dass es ein Python Script geben wird, welches jedesmal ausgeführt wird sobald man den Pico einsteckt. Dieses Python Programm verwandelt den von uns geschriebenen Duckyscript code in python, so dass er vom pico interpretiert und ausgeführt werden kann.

Für die Installation besuchen wir folgende [Website](https://circuitpython.org/board/raspberry_pi_pico/), und laden uns CircuitPython in der aktuellsten Version als .UF2 Datei herunter.

![circuitpython](img/Pico_cpdownload.PNG)

Jetzt öffnen wir den Windows File Explorer. Dabei fällt auf das der Pico als Festplatte erkannt wird:

![Picodrive](img/Pico_pico.PNG)

Wir öffnen den Datenträger des Pico, und in einer zweiten Instanz einen Download ordner, aus diesem nehmen wir nun das vorher heruntergeladene File, und verschieben es ins Pico Laufwerk:

![cpmove](img/Pico_copy.PNG)

Das Gerät verschwindet kurz, und taucht dann unter neuem namen wieder auf. Hat das alles geklappt wäre die installation von circuitpython schon beendet.

### HID Library
Damit der Pico auch tatsächlich als HID Device, in unserem fall also als Tastatur erkannt werden kann, benötigt man die entsprechende Library, welche [hier](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/tag/20220504) heruntergeladen werden kann.

![lib](img/Pico_bundle.PNG)

Das Zipfile enthält neben der HID Library noch diverse andere Bibliotheken, man muss unter umständen also erst etwas suchen, bis man die passende gefunden hat, sobald das aber der Fall ist, kopiert man sich den Ordner und fügt ihn ins Lib Verzeichnis von Circuitpy ein, das ganze sollte etwa so aussehen:

![picohid](img/Pico_hid.PNG)

![picocphid](img/Pico_copyhid.PNG)

Nun verfügt dieser über die notwendige Library und wir können einen Schritt weitergehen.

### Ducky in Python
<!-- Script uustuusche -->
Die Filestruktur auf dem Pico sollte momentan etwa so aussehen:

![picofs](img/Pico_copydip.PNG)

`code.py` wird jedesmal ausgeführt wenn man den Pico irgendwo anschliesst, wir wollen es also durch ein Script ersetzen, welches Duckyscript in Python umwandelt, und dann ausführt.
Dieses Programm kann [hier](https://github.com/dbisu/pico-ducky/blob/main/duckyinpython.py) gefunden werden, man braucht es sich also nur herunterzuladen und umzubennen, am schluss kann man das Original löschen, und die neu erstellte version auf den Pico rüberkopieren.

### Payloads
<!-- Nur zeige wie ufbaut, was mer beachte söt -->
Soweit wäre nun alles vorbereitet, das einzige was noch fehlt sind die effektiven Programme die später beim einstecken des Pi's ausgeführt werden. Diese können sich in ihrer komplexität natürlich sehr stark unterscheiden, der grundsätzliche aufbau, oder die wichtigsten Befehle, sind allerdings immer dieselben. 

![Example](img/Pico_examplescriptt.jpg)

Befehl | Auswirkung
-------- | --------
GUI r  | Öffnet Windows Run Fenster
DELAY  | Verzögert ausführung des nächsten Befehls um x millisekunden
STRING | Schreibt Text
ENTER  | Simuliert Tastendruck von Enter, geht auch mit anderen Tasten

Eigentlich ist also doch alles recht simpel aufgebaut, das einzige was es vielleicht zu beachten gibt ist dass man überall genügend Delays einbaut, ansonsten versucht der Pico nämlich so schnell alles einzutippen dass Windows vielleicht garnichtmehr hinterherkommt, also dass z.B. gewisse Fenster noch nicht aufgegangen sind, der Pi aber trotzdem schon irgendwas eintippt, nur dann halt an den Falschen ort.

Falls man sich noch etwas genauer für die Syntax interessiert wird man [hier](https://docs.hak5.org/usb-rubber-ducky-1/the-ducky-script-language/ducky-script-quick-reference) fündig.

### Tastaturlayout ändern
Damit auch tatsächlich sichergestellt wird dass die Programme welche man geschrieben hat, richtig abgetippt werden, und keine Fehler entstehen, sollte man das Tastaturlayout vom Pico auf Schweizerdeutsch ändern. Dazu geht man auf diese [Seite](https://www.neradoc.me/layouts/), fügt folgende URL ein, drückt auf "Make Zip Bundle Links" und lädt sich das Zip Archiv herunter: https://kbdlayout.info/kbdsg

![layout](img/Pico_layout.PNG)

Wurde dieser Entpackt sieht man 3 Dateien, wir brauchen einmal `keyboard_layout_win_sg.py` und `keycode_win_sg.py`. Diese kopieren wir, und fügen sie erneut in den Circuitpy lib ordner ein, er sollte nun so aussehen:

![cplibb](img/Pico_cplibb.PNG)

Bevor das ganze funktionieren kann, müssen in der code.py Datei noch ein paar wenige Anpassungen gemacht werden, die abschnitte mit dem us Tastaturlayout kommentiert man aus, und aktiviert dafür diejenigen die als Template für eine eigene Sprache dienen, statt LANG schreiben wir jeweils sg.
Das fertig angepasste Script sollte folgendermasen aussehen:

![modcode.py](img/Pico_mod.PNG)

## Test
<!-- Templates zeige, zeige wie usfühere -->
Ob alles geklappt hat, und ob auch tatsächlich alles automatisch ausgeführt wird, sehen wir sobaldwir ein Duckyscript file haben, welches so mit Inhalt befüllt wurde dass etwas bestimmtes passieren soll. Dieses speichert man nun unter dem Namen **payload.dd** auf dem Pico ab, die Namenskonvention sorgt dafür dass es erkannt und gestartet wird.

Ist man zu unkreativ um eigene Programme zu schreiben gibt es auf [Github](https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads) eine riesen auswahl an möglichen Payloads, welche einem bereits pfannenfertig serviert werden, man braucht sie also nurnoch herunterzuladen und rüberzukopieren. 

![payloads](img/Pico_payloads.PNG)

Wie auf dem Bild zu erkennen sind darunter eher harmlose sachen, wie z.B. ein Weihnachstbaum der im Texteditor gezeichnet wird, aber auch dinge die doch etwas ernsthafter sind, wie etwa eine reverse shell.

Sobald ein lauffähiges Script mit dem Namen payload.dd vorbereitet wurde kann man den Pico ausstecken und wieder einstecken, es wird nun direkt ausgeführt. Die schwierigkeit besteht darin dass es dies jedes mal tut, es kann unter umständen also mühsam werden anpassungen daran vorzunehmen, wenn man aber einen genug grossen Delay eingebaut hat, kann man schnell noch mit z.B. CTRL + C abbrechen, oder den Computer zumindest so steuern dass alle befehle irgendwo abgesetzt werden wo nichts passiert, z.B. im Editor.

## Probleme
<!-- Layout -->
Zuerst hatten wir Probleme mit dem Tastaturlayout, alle Befehle wurden von uns zwar über die Schweizer Tastatur Programmiert, aber von einer Amerikanischen Interpretiert, was natürlich falsch ist, und dazu führte dass einige Befehle nicht funktioniert haben, beziehungsweise halt nicht erkannt wurden. Nachdem wir das Layout wie gezeigt geändert haben, funktioniert aber alles tip top. Für alles zusammen haben wir vielleicht **3 Stunden** gebraucht, das eigentliche aufsetzen ist in der tat keine grosse sache, allerdings mussten wir uns ersteinmal mit dem Pico und dessen funktionsweise vertraut machen, ehe wir mit der Realisierung beginnen konnten, und die dokumentation zieht sich leider auch immer noch ein bisschen. Nichts desto trotz sind wir zufrieden mit dem Ergebnis, und freuen uns wieder etwas neues gelernt zu haben.

## Quellen
- https://youtu.be/e_f9p-_JWZw
- https://github.com/dbisu/pico-ducky
- https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads
- https://www.neradoc.me/layouts/
- https://kbdlayout.info/kbdsg
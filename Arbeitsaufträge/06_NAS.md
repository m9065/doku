<!--

Tim Schefer, Justin Barthel
Auftrag 06 NAS
Date: 18.03.2022
-->

# Dokumentation W06 - PI-NAS

 - Datum: 18.03.2022
 - Name: Tim Schefer, Justin Barthel
 - [Link zum Arbeitsauftrag](https://bscw.tbz.ch/bscw/bscw.cgi/32901611?op=preview&back_url=27527751)

## Festplatte mounten und einrichten
- Device Pfad: /dev/sda1
- UUID: 48404D3A404D2FCC

## Fragen Script

#### Was unterscheidet ein NAS von einer reinen Festplatte?
Im Gegensatz zu einer reinen Festplatte ist ein NAS im Netzwerk angebunden, es können also mehrere Leute gleichzeitig darauf zugreifen,
und keiner muss die Festplatte(n) physisch im eigenen Desktop PC eingebaut haben. Ausserdem bietet ein NAS viele zusätzliche funktionalitäten
die man auf dem 0815 Desktop PC vielleicht nicht unbedingt hat, vom automatischen Backup bis zum RAID-System lässt sich einiges konfigurieren.

#### Welche Verbindungsprotokolle gibt es, womit man auf einen Dateien-Server zugreifen kann?
- SMB / CIFS
- NFS
- iSCSI
- FTP

#### Raid Levels

- Was ist RAID Überhaupt?
RAID ist eine abkürzung für **R**edundant **A**rray of **I**ndependent **D**isks, wie der Name schon sagt, zielt die Technologie darauf ab
mehrere Festplatten miteinander zu verbinden, um Datenverluste zu vermeiden(ausser RAID 0). Um dieses Ziel zu erreichen gibt es verschiedene
Herangehensweisen, die jeweils ein bisschen anders umgesetzt werden, die sogenannten RAID Levels, die gängigsten werden jetzt gleich noch
etwas genauer erläutert.

**RAID 0**
![Funktionsweise RAID 0](img/Raid0.PNG)
- Aufbau: Es werden mindestens 2 Festplatten benötigt. Die Daten werden dabei auf die Festplatten verteilt, somit ist es das einzige RAID
Level welches nicht auf Ausfallsicherheit sondern auf Geschwindigkeit getrimmt ist, da man mit allen Platten gleichzeitig lesen / schreiben kann.
Wenn eine Festplatte aussteigt ist das komplette Array kaputt, und alle Daten sind verloren.
- Vorteile: Extrem schnell
- Nachteile: Keine Ausfallsicherheit
- Speichergrössen: 100% der Kapazität kann genutzt werden

**RAID 1**
![Funktionsweise RAID 1](img/Raid1.PNG)
- Aufbau: Es werden mindestens 2 Festplatten benötigt. Die Dateien werden dabei 1 zu 1 gespiegelt, im minimalaufbau liegt also exakt
dasselbe auf Disk 1 wie auf Disk 2.
- Vorteile: "Schnell", Ausfallsicher
- Nachteile: Tiefe Nutzungskapazität
- Speichergrössen: 50%, alles wird Gespiegelt

**RAID 5**
![Funktionsweise RAID 5](img/Raid5.PNG)
- Aufbau: Es werden mindestens 3 Festplatten benötigt. Dabei werden die Daten auf alle Festplatten verteilt. Zusätzlich wird ein sogenannter
Parritätswert errechnet und gespeichert. Dieser dient, im falle eines ausfalls dazu, die verlorenen Daten errechnen, man hat also im Gegensatz
zu RAID 1 keine direkte Kopie, kennt aber den weg um die Daten wiederherstellen zu können. Das Verfahren benötigt zwar eine Festplatte weniger
als RAID 1, benötigt dafür aber mehr Rechenleistung, weil der Paritätswert für alle Daten ausgerechnet werden muss. 
- Vorteile: Ausfallsicherheit, weniger Festplatten als RAID 1
- Nachteile: Langsame Schreibgeschwindigkeit
- Speichergrössen: 67% - 94% (Gesamtkapazität - 1 Laufwerk), Maximal 16 Festplatten

**RAID 6**
![Funktionsweise RAID 6](img/Raid6.PNG)
- Aufbau: Bei einem RAID 6 benötigt man mindestens 4 Festplatten. Anstelle eines einzigen Paritässchemas, verwendet RAID 6 zwei Schemata («P» und «Q»).
Das heisst, es können maximal 2 Festplatten ausfallen, ohne dass Daten verloren gehen.
Das Speichern von Daten ist aufwendiger als bei RAID 5, da jedes Mal 2 Paritätsaktualisierungen berechnet und geschrieben werden müssen.
Die Lesegeschwindigkeit ist gleich schnell wie RAID 5, es eignet sich daher besonders für Archivierungssysteme bei denen viel gelesen, aber wenig geschrieben wird.
Zusammengefasst könnte man sagen dass RAID 6 RAID 5 auf Steroiden ist.
- Vorteile: Hohe Ausfallsicherheit
- Nachteile: Sehr langsame Schreibgeschwindigkeit
- Speichergrössen: 50% - 88% (Gesamtkapazität - 2 Laufwerke), Maximal 16 Festplatten

**RAID 10**
![Funktionsweise RAID 10](img/Raid10.PNG)
- Aufbau: Es werden mindestens 4 Festplatten benötigt. Raid 10 vereint RAID 0 und RAID 1 miteinander, zuerst werden die Daten wie bei RAID 0 aufgeteilt,
und anschliessend gespiegelt abgespeichert (2x RAID 1).
- Vorteile: Schnell und ausfallsicher, vereint Vorteile der beiden Levels
- Nachteile: Nutzungskapazität gering, Kostenintensiv
- Speichergrössen: 50%

**JBOD**
![Funktionsweise JBOD](img/JBOD.PNG)
- Aufbau: JBOD steht für **J**ust a **B**unch **o**f **D**isks, die Technik ähnelt zwar beiden Kategorien von RAID Levels (1-10 & 0), ist aber dennoch etwas anders.
Ein JBOD verbund gewährleistet nämlich keine Ausfallsicherheit, und die Geschwindigkeit erhöht sich auch nicht, das einzige was tatsächlich passiert, ist dass mehrere
physische Festplatten zu einem Logischen Laufwerk zusammengefasst werden, und somit einfacher handzuhaben sind.
Es wird eine Platte nach der anderen mit Daten aufgefüllt, das Ziel ist die bestmögliche Ausnutzung der Gesamtkapazität. 
- Vorteile: Schnelle Geschwindigkeiten & einfache Architektur
- Nachteile: Keine Ausfallsicherheit
- Speichergrössen: 100%

#### Welche Raid Typen eignen sich für unsere Konfiguration?
- Raspberry Pi 3B, NTFS Speicher, 4x USB 2.0
Je nach Anwendungsfall und Ziel das man erreichen will, können natürlich unterschiedliche RAID Level sinnvoll sein. Aus dem Bauch heraus würde ich entweder mit
JBOD oder RAID 1 gehen, da die anderen entweder zu komplex / teuer sind, oder eine hohe Rechenleistung erfordern, die der Raspi vermutlich nicht leisten kann,
theoretisch wären aber alle möglich. Am schönsten wäre vielleicht fast RAID 10, da nichts berechnet werden muss, und man trotzdem die sicherheit hat, es scheint
mir jedoch fast etwas zu Overkill für den Raspi.


## Optionale Protokolle
TODO

<!-- Unsere Quellen Teil -->
## Quellen
 - https://www.globalsystem.ch/ratgeber/raid-systeme-erklaert/

<!--
Tim Schefer, Justin Barthel
Auftrag 05 NTP
Date: 20.03.2022
-->

# Dokumentation W05 - NTP

 - Datum: 20.03.2022
 - Name: Tim Schefer, Justin Barthel
 - [Link zum Arbeitsauftrag](https://bscw.tbz.ch/bscw/bscw.cgi/32696685?op=preview&back_url=27527718)

## Kontrollfragen
 - **Was ist eine Schaltsekunde? Warum braucht es diese?**
Eine Schaltsekunde ist eine zusätzliche Sekunde, die etwa alle 18 Monate eingespielt wird, die Zeit ändert von 23:59:59 nicht auf 00:00:00 sondern es ist ausnahmsweise 23:59:60 bevor der neue Tag anbricht. Schaltsekunden sind notwendig weil Atomsekunden, nach heutiger Definition, nicht mehr an die Erdrotation gekoppelt sind, und es zu abweichungen kommen kann, da sich die Erde nicht immer gleich schnell dreht. Ohne sie würde sich der "Sonnentag" immer weiter von der Uhrzeit entfernen, um 12 Uhr Mittags wäre die Sonne nicht mehr an ihrem höchsten Punkt, und irgendwann könnte es um diese Zeit sogar dunkel sein (übertrieben).

 - **Beschreiben Sie in eigenen Worten wie NTP funktioniert.**
Das **N**etwork **T**ime **P**rotocol synchronisiert, wie der Name schon sagt, die Zeit zwischen Zeitservern oder schlussendlich clients. Dabei gibt es verschiedene Strata die Angeben wie genau die jeweilige NTP Quelle ist.

![NTP Stratum dings](img/NTP.PNG)

Das genauste Level ist dabei Stratum 0, Geräte auf dieser Schicht liegen niemals falsch, hier findet man z.B. Atom oder Funkuhren. 
Bezieht der lokale Server die Zeit von einem Stratum-0-Gerät, wird er zur nächst ungenaueren Instanz, zum Stratum-1-Server, man kann also sagen dass jedes Strata zugleich Server für die Tieferliegende Schicht, als auch Client der höheren Schicht ist.
Die funktionsweise der tatsächlichen Synchronisation ist relativ komplex, und kann im Auftrag nachgelesen werden, grob gesagt funktioniert das ganze so dass der Client am Ende mehrere Zeitstempel hat, aus denen er sich dann selbst die aktuelle Zeit errechnen kann, man holt also nicht die effektive Uhrzeit vom Zeitserver, sondern nutzt ihn lediglich als Referenz um sich diese dann selbst kalkulieren zu können.

## Fragen anhand der `ntpq -pn` ausgabe
![Screenshot vom output](img/NTPstat.PNG)
 - **Welcher ntp-server wird vom Raspi verwendet (hostname)? Wo können sie das ablesen?**
 Aktuell wird `212.51.144.44` mit dem Hostnamen `3.ch.pool.ntp.org` als NTP Server verwendet. Erkennen kann man das mithilfe des Sterns(*) links vom Eintrag. 

 - **Welches Stratum-Level hat dieser? Wo können sie das ablesen?**
 Wenn man sich die Spalte `st` anschaut, erkennt man dass er über das Stratum Level 1 verfügt.

 - **Was bedeuten die Spalten _delay_ und _jitter_?**
   - Delay: Durchschnittliche  Verzögerung für Antwort in Millisekunden
   - Jitter: Wie regelmässig Pakete ankommen, Laufzeitschwankung zwischen den Anfragen in Millisekunden




<!-- Unsere Quellen-->
## Quellen
 - Script
 - https://www.youtube.com/watch?v=RuPQsqZaq8A
 - https://www.pool.ntp.org/zone/ch
 - https://www.ip-insider.de/was-ist-jitter-a-651837/
 

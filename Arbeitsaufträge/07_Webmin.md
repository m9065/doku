<!--

Tim Schefer, Justin Barthel
Auftrag 07 Webmin
Date: 11.03.2022
-->

# Dokumentation W07 - Webmin

 - Datum: 11.03.2022
 - Name: Tim Schefer, Justin Barthel
 - [Link zum Arbeitsauftrag](https://bscw.tbz.ch/bscw/bscw.cgi/27527809)

## Was kann Webmin?
Webmin ist ein in Perl geschriebenes Open Source Webgui, welches einem erlaubt seinen Raspberry Pi, oder allgemein Unix basierte
Systeme zu administrieren. Man kann benutzer erstellen und verwalten, Netzwerkeinstellungen konfigurieren, und so weiter. Grundsätzlich
ist es ein GUI für den Raspberry Pi, welches über den Webbrowser erreichbar ist.

<!-- Webmin installation Teil -->
## Webmin installation
Vorab müssen wir noch zwei Libraries für PERL installieren,
Webmin braucht diese später
```
sudo apt-get install libnet-ssleay-perl libio-socket-ssl-perl
```
Da der wget befehl bei uns nicht funktioniert hat haben wir das .tar.gz file [Hier](https://sourceforge.net/projects/webadmin/files/webmin/1.990/webmin-1.990-minimal.tar.gz/download) heruntergeladen, und via scp auf den pi kopiert

```
scp C:\Users\PC\Downloads\webmin-1.990.tar.gz pi@172.16.17.40:/home/pi
tar -zxvf webmin-1.990-minimal.tar.gz
cd webmin-1.990
sudo ./setup.sh
```
Jetzt wird man nach einigen sachen gefragt, die man alle auf den standartwerten lassen kann.

Wenn die installation fertig ist, erscheint eine URL, über die man auf das Webgui zugreifen kann, in unserem Fall https://himbeeri:10000
Dieses sieht so aus:
![Webgui](img/01_Webmingui.PNG)

Um nun die Notwenidigen Module zu installieren muss man folgende schritte ausführen:
 - Unter "Webmin Configuration" auf "Webmin Modules gehen"
 - Option Standart Module from www.webmin.com wählen und das Modul UserAdmin sowie drei optionale auswählen
 - Über Install Module alles installieren


<!-- User Administration Teil -->
## User Administration
Für die User Administration muss man ein Modul herunterladen.
Diese findet man im Start Menu unter **Webmin Configuration >> Webmin Modules.**

![Modules](img/02_Webmin-modules.PNG)

**Wichtig!**
Hier muss man nun auswählen, welches Modul man nimmt. Da es eine riesige Auswahl hat an verschiedenen Modulen.
Für die User Administration braucht man:
- UserAdmin

Dann sollte es etwa so aussehen:

![Modules](img/03_Webmin-Useradmin.PNG)

Sobald man das Modul installiert hat kann man auf das User Admin Modul gehen und dort die verschiedenen User anlegen.

Dies sieht das in etwa so aus:

![Modules](img/05_User-erstellen.PNG)

Sobald man nun die **Adminrechte** vergeben hat und den Auftrag erledigt hat, sollte der Admin User in Kursiv dargestellt sein. Da man diesen Disabled hat.

![Modules](img/04_User.PNG)

<!-- Zusatz Module Teil -->
## Zusatz Module
 - Shell
![Shell](img/05_Shell.PNG)
 Die im Webmin integrierte Shell kann nützlich sein, wenn man sich nicht jedes mal via ssh oder so auf den Pi verbinden möchte.
 Im Prinzip gibt es aber nicht viel dazu zu sagen, es ist ganz normal bash, wie auf den meisten Unix basierten Systemen.
 
  - Net
![Net](img/03_Netmodule.PNG)
Wie auf dem Bild zu erkennen ist, erlaubt einem dieses Modul diverse Netzwerkeinstellungen vorzunehmen. Ich persönlich finde dies sehr praktisch,
da solche Operationen über die Kommandozeile manchmal etwas mühsam sind.

 - File Manager
![Filemanager](img/04_Filemanager.PNG)
 Über den Filemanager kann man sich ganz normal durchs Dateisystem des Raspis klicken. Dies könnte vor allem für User interessant sein, die eher weniger Erfahrung
 in der Unix welt haben, aber trotzdem auf die installation eines GUIs auf OS Level verzichten möchten. So müssen sie nicht zwingend alle Bash commands kennen,
 sondern können wie gewohnt mit der Maus navigieren.
 
<!-- Testing Teil -->
## Testing
 - Webmin starten / stoppen
```
cd /etc/webmin
sudo ./start
sudo ./stop
```

![StartStop](img/02_StartStop.PNG)

<!-- Unsere Quellen Teil -->
## Quellen
 - http://www.webmin.com/

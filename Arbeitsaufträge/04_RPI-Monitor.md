<!--
Tim Schefer, Justin Barthel
Auftrag 04 RPI-Monitor
Date: 25.03.2022
-->

# Dokumentation W04 - RPI-Monitor

 - Datum: 24.03.2022
 - Name: Tim Schefer, Justin Barthel
 - [Link zum Arbeitsauftrag](https://bscw.tbz.ch/bscw/bscw.cgi/33590485?op=preview&back_url=27527694)

## Was ist RPI-Monitor?
Der Rpi-Monitor ist ein Monitoring Tool, welches einem, ähnlich wie [Webmin](https://gitlab.com/m9065/doku/-/blob/main/Arbeitsauftr%C3%A4ge/07_Webmin.md), erlaubt, per Browser wichtige Systeminformationen auszulesen.
Über `localhost:8888` kann man darauf zugreifen, und sieht direkt das wichtigste, ähnlich wie wenn man den `htop` befehl ausführen, oder den Taskmanager öffnen würde.

Im ganzen gibt es folgende 5 Kategorien, wobei vor allem die ersten 3 Spannend sind:

![Übersicht](img/RPI-Monitor-Overview.PNG)

Unter **Status** bekommt man Infos zu
 - Version des Betriebssystems
 - Uptime
 - Hardwareauslastung

![Status](img/RPI-Monitor-Status.PNG)

Bei **Statistics** sieht man in einem kleinen Graphen nochmals mehr oder weniger dieselben Infos, allerdings über längere Zeit dargestellt, und man hat die möglichkeit gezielter zu Filtern wenn man nach etwas konkretem sucht.

![Statistic](img/RPI-Monitor-Statistic.PNG)

## Add Ons
Im Obigen abschnitt wurden an sich schon alle Grundfunktionalitäten des RPI-Monitors gezeigt, durch Addons bekommt man allerdings die Option, das GUI noch mit "eigenen" features zu ergänzen.
Wir haben z.B. noch die Erweiterung ShellInABox installiert, diese ermöglicht es einem direkt über den Browser eine SSH verbindung zum Pi aufzubauen, wie im Bild zu erkennen ist:

![Addons](img/RPI-Monitor-Addons.PNG)

Das ist unter umständen ganz praktisch, da man sich nicht immer von Hand verbinden muss, und erst recht keinen Monitor benötigt, man kann sich z.B. ein Bookmark einrichten, und mit einem klick befindet man sich auf der Kommandozeile.

<!-- Unsere Quellen-->
## Quellen
 - Script
 - https://github.com/XavierBerger/Rpi-monitor
 - https://bscw.tbz.ch/bscw/bscw.cgi/29337134?op=preview&back_url=27527694
 - https://xavierberger.github.io/RPi-Monitor-docs/25_addons.html?highlight=shell
 
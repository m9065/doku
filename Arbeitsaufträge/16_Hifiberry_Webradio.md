<!--
Tim Schefer, Justin Barthel
Auftrag 16 HiFiberry & Webradio
Date: 26.03.2022
-->

# Dokumentation W16 - HiFiberry & Webradio

 - Datum: 26.03.2022
 - Name: Tim Schefer, Justin Barthel
 - [Link zum Arbeitsauftrag](https://bscw.tbz.ch/bscw/bscw.cgi/32930692?op=preview&back_url=27528230)

## HiFiberry
[HiFiberry](https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/) ist ein Unternehmen welches primär verschiedenste Soundkarten für den Raspberry Pi herstellt.

Da wir den Auftrag von zuhause aus gelöst haben, und noch kein HiFiberry Modul beziehen konnten, waren wir bis jetzt nicht in der Lage es in Betrieb zu nehmen, die Folgenden Punkte wurden also alle mit dem Onboard Soundchip vom Raspi durchgeführt, nichts destro trotz wollen wir noch kurz auf die einrichtung von HifiBerry eingehen.

Wie sie genau funktioniert kann man [hier](https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/) nachlesen, wie ich finde sieht das ganze nicht allzu kompliziert aus, wir sind allerdings wie gesagt noch nicht zum ausprobieren gekommen.
Als erstes muss man einige kleine änderungen in der `/boot/config.txt` Datei vornehmen, dabei geht es vor allem darum den Onboard sound zu deaktivieren und, je nach modell, konfigurationen für den DAC einzustellen.
Hat man dies getan kann mit dem Befehl `aplay -l` getestet werden welche soundkarte aktiviert ist, wenn dort irgendwas mit HifiBerry steht ist das schonmal ein gutes Zeichen. Um 100% sicherzugehen ob man tatsächlich auch Ton ausgeben kann würde ich jetzt kurz versuchen ein eigenes Audiofile abzuspielen, das ist mit `aplay song.wav` möglich.
War der Soundcheck erfolgreich kann man zum nächsten Schritt übergehen, falls nicht würde ich nochmals das Configfile prüfen, und schauen ob die richtigen Treiber etc. für die Soundkarte definiert wurden. Hat man keine HiFiberry soundkarte kann dieser Schritt sowieso ganz übersprungen werden, die Onboard karte ist schon von haus aus eingerichtet.

## Mplayer
Mplayer ist ein Multimedia Programm, über das sich unter anderem Videos oder Audiodateien abspielen lassen. Das klingt erstmal nicht sehr besonders, es gibt tausende konkurrenten, wie z.B. VLC, die genau dasselbe machen, der mplayer hebt sich aber durch ein cooles feature ab, was die meisten anderen nicht bieten können, nämlich handelt es sich dabei um ein open source Kommandozeilenprogramm. Im Falle vom mplayer bedeutet dass das weniger Ressourcen verwendet werden, und man sehr flexibel ist, es wäre z.B. möglich eigene kleine Scripte rund um die Applikation zu schreiben usw.

### Praxisbeispiele
Wenn man etwas genauere infos zur anwendung des tools möchte, sieht man sich mit `man mplayer` am besten kurz die manpage an, hier aber trotzdem noch einige beispiele, wie man den mplayer verwenden kann.

**Syntax:**

**mplayer** [Optionen] [Datei|URL|Playlist|-]

**Video/Audiofile abspielen**

`mplayer /path/to/file`

**Playlist abspielen**

`mplayere -playlist mylist.txt`

**Video ohne Bild wiedergeben**

`mplayer -novideo something.mpg`

**Vom Netzwerk streamen**

`mplayer http_proxy://proxy.micorsops.com:3128/http://micorsops.com:80/stream.asf`

**Vom Netzwerk streamen & speichern**

`mplayer http://217.71.208.37:8006 -dumpstream -dumpfile stream.asf`

**DVD Playback**

`mplayer dvd://1 -dvd-device /dev/hdc`


## Installation Internetradio
Internetradio zu hören ist an sich keine schwere sache, es kann lediglich etwas tricky sein die passende URL zu finden, aber hier folgt trotzdem eine kleine schritt für schritt Anleitung:

1. **Installation mplayer (falls nicht schon vorhanden)**

```
sudo apt update
sudo apt install mplayer
```
2. **Radio URL finden**

Am besten schaut man kurz im Internet nach wie die URL für den Sender lautet, den man gerne hören möchte, wir haben hier eine kleine [Liste](https://bommag.ch/uploads/pdf/Internet-Radio%20direkte%20Streams.pdf) mit den gängigsten Schweizer sendern gefunden.

Bei diesem Schritt sollte man besonders achtsam sein, da sich die URL die im Browser erscheint wenn man über diesen Radio hört, von der unterschiedet die mplayer zur wiedergabe benötigt. Bei SRF3 ist die "Browser URL" z.B. `https://www.srf.ch/play/radio/livepopup/radio-srf-3` während mplayer diese hier `http://stream.srg-ssr.ch/m/drs3/mp3_128` braucht.

3. **Radio wiedergeben**

Dies kann mit dem Befehl `mplayer <Radio-URL>` gemacht werden

4. **Script erstellen (optional)**
Damit man sich nicht den kompletten Befehl mit der etwas längeren URL merken muss, erstellen wir noch ein kleines Radio Script. Dazu erstellen wir zuerst folgende Datei:

```shell
sudo touch radio.sh
sudo nano radio.sh
```
In diese schreiben wir dann den Einzeiler um die Radiowiedergabe zu starten:

```shell
#!/bin/bash
mplayer http://stream.srg-ssr.ch/m/drs3/mp3_128
```

Zum Schluss muss das Script noch ausführbar gemacht werden, und schon ist es funktionstüchtig:
```shell
sudo chmod +x radio.sh
./radio.sh
```

Wenn alles geklappt hat, sollte die Konsole etwa so aussehen:

![Radio](img/Radio.PNG)

## Mopidy
Mopidy ist ein Musikserver, die Idee dahinter ist es dass man sich mehr oder weniger selbst ein kleines Webgui zusammenstellen kann, welches einem dann erlaubt Musik zu verwalten, entweder nur lokal oder vielleicht sogar im ganzen Haus, alles ist sehr modular und flexibel aufgebaut.

Die installation läuft folgendermassen ab:

1. GPG keys hinzufügen

```shell
sudo mkdir -p /usr/local/share/keyrings
sudo wget -q -O /usr/local/share/keyrings/mopidy-archive-keyring.gpg \
  https://apt.mopidy.com/mopidy.gpg
```

2. apt Repository zu Paketquellen hinzufügen
```shell
sudo wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/buster.list
```

3. Mopidy installieren
```shell
sudo apt update
sudo apt install mopidy
```

Mopidy ist jetzt installiert, und kann mit `mopidy` gestartet werden. Wenn man das zum ersten mal macht werden einige Dateien und Ordner erstellt, wesshalb es etwas länger gehen kann, die wichtigste aller konfigurationsdateien befindet sich unter `~/.config/mopidy/mopidy.conf` und ist in mehrere Abschnitte aufgeteilt die via default auskommentiert sind. Will man das Webgui erreichen sollte zumindest die http sektion bearbeitet werden

![Mopidy](img/MopidyConf.PNG)

Nachdem man die Konfigurationen angepasst hat muss mopidy noch kurz neugestartet werden, und das GUI sollte, wie auf dem Bild zu sehen, erreichbar sein:

![Mopidy GUI](img/MopidyGui.PNG)

### Extensions
Der Server läuft jetzt zwar grundsätzlich, allerdings ist noch nichts darauf installiert. [Hier](https://mopidy.com/ext/) sieht man alle ofiziellen Extensions die man sich individuell installieren kann. 

**Beispiel Spotify**
In unserem Beispiel wollen wir [Spotify](https://mopidy.com/ext/spotify/) installieren. Das geht mit `sudo apt install mopidy-spotify`, anschliessend dazu muss man sich auf der eben verlinkten Seite mit seinem Spotify account authentifizieren, um eine client_id und ein client_secret zu bekommen.

Zuletzt muss man noch folgende Zeilen zu mopidy.conf hinzufügen
```shell
[spotify]
username = alice
password = secret
client_id = ... client_id value you got from mopidy.com ...
client_secret = ... client_secret value you got from mopidy.com ...
```
den Server neustarten, und man sollte auf dem GUI  alles erkennen können. Uns ist das leider nicht ganz gelungen, beim Starten des Servers kommt die Meldung dass man über einen Spotify Premium account verfügen muss, um das feature nutzen zu können, was ich nicht habe.

![Mopidy Error](img/MopidyError.PNG)

Auch als wir es mit anderen Diensten, wie z.B. Soundcloud versucht haben ist immer etwas fehlgeschlagen, und das Webgui hat sich nicht verändert, es ist uns leider nicht gelungen dies zu beheben. Theoretisch wissen wir wie Mopidy funktionieren würde, konnten es in unserem Fall aber leider nicht anwenden, hoffentlich bekommen wir wenigstens den einen Punkt für die Dokumentation :-)

## Quellen
 - https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/
 - https://wiki.ubuntuusers.de/MPlayer/
 - https://docs.mopidy.com/en/latest/installation/raspberrypi/
 - https://mopidy.com/ext/
 - https://mopidy.com/ext/spotify/
 - https://github.com/mopidy/mopidy-spotify


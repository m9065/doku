# Dokumentation Printserver

 - Datum: 08.04.2022
 - Name: Tim Schefer, Justin Barthel

## Fixe IP Konfigurieren
Damit der Printserver zuverlässig funktionieren kann, ist es notwendig eine Statische IP Adresse zu konfigurieren. Dazu verbinden wir uns über VNC auf die Grafische Oberfläche, und rechtsklicken oben auf das Internet Symbol, wo übers Kontextmenü die Option `Wireless & Wired Network Settings` geöffnet werden kann.
![ETH uplink](img/Pihole_Link.PNG)

Dort wählt man dann das gewollte Interface aus, auf der die Statische Adresse eingerichtet werden soll, in unserem Fall eth0, da wir den Pi übers Kabel mit dem Netz verbunden haben. Dann definieren wir die IP welche verwendet werden soll, hier `172.16.17.40` und den Standartgateway, beziehungsweise den Router, die anderen Felder lassen wir erstmal leer.
![StaticIP](img/Pihole_Static.PNG)

Somit war es das auch schon, der Pi sollte nun immer, auch nach einem Neustart ein paar Tage später die .40er Adresse zugewiesen bekommen.

## Installation Cups
Mit folgenden Befehlen lassen  sich einmal cups(die Server komponente) und cups-client(die Client komponente) installieren.
```bash
sudo apt update
sudo apt install cups cups-client
```
Ansonsten werden noch einige andere Treiber benötigt die wir uns ebenfalls herunterladen:
```bash
sudo apt install hplib printer-driver-hpijs printer-driver-gutenprint
```

Die reine installation ist somit auch schon beendet, und der Dienst kann wie folgt gestartet werden.
```bash
sudo /etc/init.d/cups start
```
## Benutzer zuweisen
Damit ein Benutzer auf dem Raspberry Pi die notwendigen Rechte zur Druckerkonfiguration bekommt, muss dieser mitglied der lpadmin Gruppe sein. Dies lässt sich über
```bash
sudo uüsermod -aG lpadmin pi
```
bewerkstelligen.

Was man ebenfalls machen könnte, ist ein separater **Drucker-Benutzer** zu erstellen. das wäre folgendermassen möglich:
```bash
sudo useradd -c Druckuser -M -s /bin/false Drucker
sudo usermod -aG lpadmin Drucker
sudo passwd Drucker
```
## Konfiguration Cups
### Raspi
Die Konfigurationsdatei von Cups liegt unter `/etc/cups/cupsd.conf` und kann ganz normal mit einem Editor, wie z.B. `nano` bearbeitet werden. Hier müssen einige anpassungen gemacht werden, damit später ein Webinterface verfügbar ist, und wir den Printserver überhaupt verwenden können. Es folgen Screenshots von den betroffenen stellen, wo man etwas ändern / eintragen sollte:

![config1](img/Printserver_config1.PNG)

![config2](img/Printserver_config2.PNG)

![config3](img/Printserver_config3.PNG)

![config4](img/Printserver_config4.PNG)

Abschliessend müssen wir den Printserverdienst noch einmal neustartet, damit auch sicher alles übernommen wird:

![restart](img/Printserver_restart.PNG)

### Webinterface
Die Grafische oberfäche ist, wie gewohnt, über die IP des Pi's, in unserem Fall `172.16.17.40:631` erreichbar. Auf dieser gehen wir zuerst zum Punkt **Verwaltung**

![homepage](img/Printserver_webgui1.PNG)

Auf diesem Reiter machen wir dan noch eine letzte Anpassung damit von extern auf den Drucker zugegriffen werden kann:

![settings](img/Printserver_webgui2.PNG)

Jetzt bestätigen wir dies mit dem **Einstellungen ändern** Knopf, und starten den Dienst über `sudo /etc/init.d/cups restart` nochmals kurz neu.

Um nun den tatsächlichen Drucker einzurichten, müssen wir ihn via USB mit dem Raspberry Pi verbinden, und im Register **Verwaltung** das entsprechende Gerät auswählen. Mit dem **weiter** Button gelangt man dann zur nächsten Konfigurationsseite.

![addprinter1](img/Printserver_webgui3.PNG)

Im nächsten Fensterkann man dem Drucker einen Namen, eine Beschreibung und einen Ort zuweisen. Zusätzlich sollte bei der Option "**Freigabe**" der Haken gesetzt werden, da der Drucker sonst nicht im Netzwerk auffindbar sein wird.

![addprinter2](img/Printserver_webgui4.PNG)

Zuletzt kann dann noch das Druckermodell welches man im Einsatz hat gesetzt werden, dies ist vor allem für die installation der Treiber relevant. Hat man dies gemacht, kann der Drucker über **Drucker hinzufügen** und **standarteinstellung festlegen** hinzugefügt werden, die Konfiguration von der Raspi seite aus wäre somit vollendet.

![addprinter3](img/Printserver_webgui5.PNG)

Wenn man jetzt zu testzwecken schonmal eine Seite ausdrucken möchte, kann man das unter dem Register **Drucker** machen. Dort einfach aufs Dropdown-Feld **Wartung** gehen und **Testseite ausdrucken** wählen, weiter unten sieht man dann auch gleich alle aufträge in der Queue.

![testseite](img/Printserver_testseite.PNG)

## Geräte verbinden
### Laptop
Auf dem Laptop muss der Internetdruckdienst aktiviert sein. Dieser lässt sich in der **Systemsteuerung** unter **Programme ->  Programme und Features -> Windows-Features aktivieren oder deaktivieren -> Druck- und Dokumentendienste** bewerkstelligen. Dort einfach dieselben Haken wie auf dem Bild setzen.

![activate](img/Printserver_activate.PNG)

Jetzt kann in den Einstellungen nach Druckern und Scannern gesucht werden, nach einer weile taucht folgendes Feld auf:

![einbinden1](img/Printserver_feld.PNG)

Wenn man dort draufdrückt hat man die möglichkeit manuell einen Drucker einzubinden, das machen wir nach folgendem Schema: `http://IP-Adresse:631/printers/Druckername`

![einbinden](img/Printserver_einbinden2.PNG)

Der Printserver ist jetzt erfolgreich hinzugefügt worden, und wir können in ganz normal in Word etc. auswählen.

### Handy (Airprint)
Auf dem Handy ist der ganze Prozess fast noch ein bisschen einfacher, zuerst muss sichergestellt werden dass man via WLAN ins selbe Netzwerk verbunden ist wie der Printserver, in unserem Fall also **TBZ-LAB_U066**

![u66](img/Printserver_LABU66.PNG)

Dann kann irgendein Dokument gewählt werden, dass man ausdrucken möchte, **wichtig** ist dass man als Printder den Raspberry Pi aus wählt, und nicht direkt über den Drucker geht (im Bild falsch dargestellt)

![airprint](img/Printserver_airprint.PNG)

## Netzwerkplan
![Netzwerkplan](img/Printserver_Netzwerkplan.PNG)

## Quellen
 - https://bscw.tbz.ch/bscw/bscw.cgi/31457854?op=preview&back_url=27527604